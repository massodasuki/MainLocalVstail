package util
{
	[Bindable]
	public class Singleton
	{
		
		/** Only one instance of the model locator **/
		
		private static var instance:Singleton = new Singleton();
		
		/** Bindable Data **/
		
		public var statusCode:String = "Singleton";
		
		public function Singleton()
		{
			if(instance)
			{
				throw new Error ("We cannot create a new instance." +
					"Please use Singleton.getInstance()");
			}
		}
		
		public static function getInstance():Singleton
		{
			return instance;
		}
	}
}