import flash.data.*;
import flash.errors.SQLError;
import flash.events.Event;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;
import flash.utils.ByteArray;
import flash.utils.getTimer;

import mx.collections.ArrayCollection;

import util.SHA1;

private var sqlconn:SQLConnection = new SQLConnection;
private var stmtParam:SQLStatement = new SQLStatement();

private function sqlInit(dbname:String):void
{
	var file:File = File.userDirectory.resolvePath(dbname);
	
	var isNewDB:Boolean = !file.exists;
	
	
	 if(isNewDB){
		var dir:File = File.userDirectory.resolvePath("Vital");
		dir.createDirectory();
		sqlConn(dbname);
	    stmtParam.sqlConnection = sqlconn;	
	 
		
		query("CREATE TABLE IF NOT EXISTS student (id_pelajar TEXT(15),nama_pelajar TEXT(130),katanama TEXT(30),katalaluan TEXT(40),gambar MEDIUMBLOB)");
		
		query("CREATE TABLE IF NOT EXISTS course (id_pelajar TEXT(15),id_subjek TEXT(15), nama_subjek TEXT(130), kod TEXT(15), keterangan TEXT)");
		
		query("CREATE TABLE IF NOT EXISTS content (id INT,course_name TEXT, video_name TEXT, thumb_image TEXT," +
			" title TEXT, duration INT, date_added TEXT,html_text TEXT, download TEXT);");
		 
		
		//query("CREATE TABLE IF NOT EXISTS studentreport (id_pelajar TEXT(15),id_subjek TEXT(15), nama_video TEXT(15), tarikh TEXT(130), masa_mula TEXT(15),"+
		//	 "masa_tamat TEXT(15), current_duration TEXT, total_duration TEXT, last_position TEXT, bilangan_view TEXT)");
		
	
		
		
	}else{
		sqlConn(dbname);
		stmtParam.sqlConnection = sqlconn;
	}
}

private function sqlConn(dbName:String):void
{
	var tbcreate:SQLStatement = new SQLStatement; 
	
	var dbPath:File = File.userDirectory.resolvePath(dbName);
	//var isNewDB:Boolean = !dbPath.exists;
	
	sqlconn.addEventListener(SQLEvent.OPEN, dbCreated);	
	sqlconn.addEventListener(SQLErrorEvent.ERROR, dbError);
	tbcreate.addEventListener(SQLEvent.RESULT, tbCreated);
	tbcreate.addEventListener(SQLErrorEvent.ERROR, tbError);
	
	sqlconn.open(dbPath,SQLMode.CREATE, false, 1024, Myvar.encryptionKey);
}

private function query(dbinsert:String):void
{
	var insertst:SQLStatement = new SQLStatement;
	insertst.addEventListener(SQLEvent.RESULT, recUpdate);
	insertst.addEventListener(SQLErrorEvent.ERROR, errorHandle);
	insertst.sqlConnection = sqlconn;
	insertst.text = dbinsert;
	insertst.execute();
}

private function sendStmt(statment:String, cls:Class ):SQLResult
{
	var stmt:SQLStatement = new SQLStatement();
	stmt.addEventListener(SQLEvent.RESULT, resultHandler);
	stmt.addEventListener(SQLErrorEvent.ERROR, errorHandle);
	stmt.sqlConnection = sqlconn;
	stmt.text = statment;
	stmt.itemClass = cls;
	stmt.execute();
	return stmt.getResult();
}

private function sendStmtParam(cls:Class):SQLResult
{
	stmtParam.addEventListener(SQLEvent.RESULT, resultHandler);
	stmtParam.addEventListener(SQLErrorEvent.ERROR, errorHandle);
	stmtParam.itemClass = cls;
	stmtParam.execute();
	return stmtParam.getResult();
}
private function dbCreated(event:SQLEvent):void
{
	trace("Database Created");
}
private function dbError(event:SQLErrorEvent):void
{
	trace("Error message:", event.error.message);
	trace("Details:", event.error.details);
}
private function tbCreated(event:SQLEvent):void
{
	trace("Table Created");
}
private function tbError(event:SQLErrorEvent):void
{
	trace("Error message:", event.error.message);
	trace("Details:", event.error.details);
}
private function recUpdate(event:SQLEvent):void
{
	trace("DB updated");
}
private function resultHandler(event:SQLEvent):void
{
	trace("DB connection ok");
}
private function errorHandle(event:SQLErrorEvent):void
{
	trace("Error message:", event.error.message);
}
