

import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.display.Bitmap;
import flash.display.Loader;
import flash.errors.SQLError;
import flash.events.Event;
import flash.events.StatusEvent;
import flash.filesystem.File;
import flash.net.URLRequest;
import flash.utils.ByteArray;

import mx.controls.Alert;
import mx.controls.Image;
import mx.graphics.codec.JPEGEncoder;
import mx.rpc.events.ResultEvent;
import mx.rpc.http.mxml.HTTPService;

private var updateService:HTTPService = new HTTPService();
private var retriveService:HTTPService = new HTTPService();
private var profileEditService:HTTPService = new HTTPService();

 
public var login:String = "";
public var processCompleted:int = 0;

 
private function loginRemote():void
{
	var loginService:HTTPService = new HTTPService();
	
	//mx.controls.Alert.show("Loading Data ...");
	btnEnter.enabled = false;  
	lblMsg.text ="Loading Data... Please Wait";
	
	loginService.url = Myvar.url+"/Vital/InitDataServlet";
	loginService.useProxy = false;
	loginService.method = "POST";
	loginService.addEventListener(ResultEvent.RESULT, loginResult);
	loginService.send(loginModel);
	
	
}

private function loginResult(e:ResultEvent):void
{
	var profileResult:Object = e.result as Object;
	 
	
	if (profileResult.init.hasOwnProperty("status"))
	{
	    if (profileResult.init.status == 0){
			mx.controls.Alert.show("Entered Login ID and Password did not match");
		}
	}
	else if (profileResult.hasOwnProperty("init")){
			if (profileResult.init.hasOwnProperty("profile"))
			{
				var idStudent:String = profileResult.init.profile.id; 
				var nameStudent:String= profileResult.init.profile.nama;
				login = profileResult.init.profile.katanama;
				var pass:String = profileResult.init.profile.katalaluan;
				 
				query("INSERT INTO student (id_pelajar,nama_pelajar,katanama,katalaluan) " +
					"values('"+idStudent+"','"+nameStudent+"','"+login+"','"+pass+"')");
				
												
				lblUser.text = nameStudent;
				
					
			}
			
			if (profileResult.init.hasOwnProperty("courses"))  
				if (profileResult.init.courses.hasOwnProperty("course"))
				{
					
					var leng:int = profileResult.init.courses.course.length;
				
					for (var i:int = 0; i<leng; i++)
					{
						var idStudentC:String = profileResult.init.courses.course[i].idpelajar;
						var idSubject:String = profileResult.init.courses.course[i].idsubjek;
						var subject:String = profileResult.init.courses.course[i].subjek;
						var code:String = profileResult.init.courses.course[i].kod;
						var summary:String = profileResult.init.courses.course[i].keterangan;
						 
						var subjekID:SQLResult = sendStmt("Select id_subjek from course where id_subjek = '"+idSubject+"'",Object);
						
						if(subjekID.data != null)
						{
							query("UPDATE course SET nama_subjek ='"+subject+"',kod ='"+code+"',keterangan ='"+summary 
								+"' WHERE id_subjek ='"+idSubject+"'");	
						}else
						{	
							query("INSERT INTO course (id_pelajar,id_subjek,nama_subjek,kod,keterangan)"+
								"values('"+idStudentC+"','"+idSubject+"','"+subject+"', '"+code+"', '"+summary+"')");
						}
					
					}
					
				}
			   	
			
			if (profileResult.init.hasOwnProperty("contents"))  
				if (profileResult.init.contents.hasOwnProperty("content"))
				{
					 
					var lengC:int = profileResult.init.contents.content.length;
					 
					for (var j:int = 0; j<lengC; j++)
					{
						var idContent:String = profileResult.init.contents.content[j].idkandungan;
						var courseName:String = profileResult.init.contents.content[j].namasubjek;
						var fileNameC:String = profileResult.init.contents.content[j].namafail;
						var thumbImage:String = profileResult.init.contents.content[j].thumb;
						var title:String = profileResult.init.contents.content[j].tajuk;
						var durationC:String = profileResult.init.contents.content[j].durasi;
						var dateC:String = profileResult.init.contents.content[j].tarikh;
						var summaryC:String = profileResult.init.contents.content[j].keterangan;
						var download:String = "true"; 
						
						var contentID:SQLResult = sendStmt("Select id from content where id = '"+idContent+"'",Object);
						
						if(contentID.data != null)
						{
						
							query("UPDATE content SET course_name ='"+courseName+"',video_name ='"+fileNameC+"',thumb_image ='"+thumbImage+"',title ='"+title+"',duration ='"+durationC+"',date_added ='"+dateC+"',html_text ='"+summaryC+"',download ='"+download 
								+"' WHERE id ='"+idContent+"'");		
							
						
						}
						else{
							query("INSERT INTO content (id,course_name, video_name,thumb_image,title,duration,date_added,html_text,download)"+
								"values('"+idContent+"','"+courseName+"','"+fileNameC+"', '"+thumbImage+"', '"+title+"', '"+durationC+"', '"+dateC+"', '"+summaryC+"', '"+download+"')");
						}	
						
					}
					
				}
			 
					
			updateData(); 
			
		} 
	
}
 
private function updatePicture():void
{
	// Once the image file has been selected, we now have to load it
	var loader:Loader = new Loader();
	loader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleLoadPicture);
	loader.load(new URLRequest(Myvar.url+"/Vital/image.jsp?id="+login));
}	


private function handleLoadPicture(event:Event):void
{
	
	// The first thing that we do is create a Loader object (which is a subclass od DisplayObject)
	var loader:Loader = Loader(event.target.loader);
	// Next, we cast the loader as a Bitmpa object, as the Bitmap object has function to return a BitmapData object based on the image
	var image:Bitmap = Bitmap(loader.content);
	var encoder:JPEGEncoder = new JPEGEncoder(100);
	// The PNGEncoder allows you to convert BitmapData object into a ByteArray, ready for storage in an SQLite blob field
	var byteArray:ByteArray = encoder.encode(image.bitmapData);
	
	
	//Myvar.url+"/VSTaiL/images/Student.jsp?id="+matric);
	stmtParam.text = "UPDATE student set gambar=:jpeg WHERE katanama = '"+login+"'";
	stmtParam.parameters[":jpeg"] = byteArray; 
	stmtParam.execute();
	//updateData(); 
	 
}