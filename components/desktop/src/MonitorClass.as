import air.net.SocketMonitor;
import air.net.URLMonitor;
import flash.net.URLRequest;
import flash.events.StatusEvent;
import flash.events.StatusEvent;

private var monitor:URLMonitor;

private function startMonitor():void
{	
	 
	monitor = new URLMonitor(new URLRequest(Myvar.url));
	monitor.addEventListener( StatusEvent.STATUS, onStatusEvent );
	//monitor.pollInterval = 500; 
	 
	monitor.start();
}
private function onStatusEvent( event : StatusEvent ) : void
{
	var evt : StatusEvent = event;
	Myvar.statusCode = evt.code;
	extMethod();
}