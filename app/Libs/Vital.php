<?php

namespace App\Libs;

use App\Models\AConfig;

trait Vital
{
    function getDate($date, $format)
    {
        $date_return = '';
        if (!$this->isEmpty($date)) {
            $date_return = date($format, strtotime($date));
        }

        return $date_return;
    }

    function isEmpty($param)
    {
        $is_empty = false;
        if (empty($param) || $param == '' || is_null($param) || !isset($param)) {
            $is_empty = true;
        }

        return $is_empty;
    }

    /**
     *
     * @param      $model
     * @param      $input
     * @param null $option With 'exclude' item to exclude post data, and 'date' to format post data if date exist
     *
     * @return array
     */
    function populateSaveValue($model, $input, $option = null)
    {
        foreach ($input as $key => $value) {
            if ($option != null) {
                if (isset($option['date'])) {
                    if (in_array($key, $option['date'])) {
                        if ($value != null) {
                            $value = date("Y-m-d", strtotime($value));
                        }
                    }
                }

                if (isset($option['exclude'])) {
                    if (in_array($key, $option['exclude'])) {
                        $key = null;
                    }
                }

                if ($key != null && isset($option['exclude_prefix'])) {
                    if (substr($key, 0, strlen($option['exclude_prefix'])) != $option['exclude_prefix']) {
                        $key = substr($key, strlen($option['exclude_prefix']));
                    } else {
                        $key = null;
                    }
                }
            }

            if ($key != null) {
                $model->$key = $value == '' ? null : $value;
            }
        }

        return $model;
    }

    public function getDbConfig($config)
    {
        $value = null;
        
        $vital_or_organization = \Auth::user()->role == 'Vital' ? \Auth::user()->user_id : \Auth::user()->organization()->id;
        $config                = AConfig::where('param', $config)->where('vital_or_organization', $vital_or_organization);
        if ($config->count() > 0) {
            $value = $config->first()->value;
        }

        return $value;
    }

    public static function getFile($asset)
    {

        return $asset->id . '.' . self::getExtension($asset->asset_name);
    }

    public static function getExtension($file)
    {
        $extension = explode('.', $file);

        return $extension[count($extension) - 1];
    }

    public static function getRatio($resolution)
    {
        $hw = explode(' x ', $resolution);

        return $hw[1] / $hw[0];
    }

    public function generateSerialNumber($user_id)
    {
        $hash = md5($user_id . substr(sha1(microtime()), 0, 22));

        for ($i = 0; $i < 1000; $i++) {
            $hash = md5($hash);
        }

        return implode('-', str_split(substr(strtoupper($hash), 0, 20), 5));
    }
}