<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'LoginController@landingPage');
Route::get('/login', 'LoginController@login');
Route::post('login', ['as' => 'login', 'uses' => 'LoginController@doLogin']);
Route::get("locale/{locale}", "LoginController@locale");
//Route::any('reset-password/{token}', 'UserController@newPassword');
//Route::any('confirm/{token}', ["uses" => "UserController@confirmUser"]);

//route untuk aktifkan pengguna
Route::group(array("prefix" => "activate"), function () {
    Route::get("validate/{username}", 'LoginController@validateUsername');
    //client
    Route::get("{id}", 'LoginController@formActivateClient');
    Route::post("{id}", 'LoginController@doActivateClient');


    /*--DELETE JIKA TIADA ERROR--
    Route::get("student/{id}", 'ClientController@showStudent');
    Route::get("staff/{id}", 'ClientController@showStaff');
    Route::get("parent/{id}", 'ClientController@showParent');*/
});

Route::group(array('middleware' => 'auth'), function () {

    //route umum
    Route::get("dashboard", "LoginController@dashboard");
    Route::get("config", 'OtherController@indexConfig');
    Route::get("config/option", 'OtherController@indexConfig');
    Route::get("profile", "LoginController@profile");
    Route::get("resend-activation/{type}/{id}", 'LoginController@resendActivation');
    Route::get("device", 'OtherController@device');
    Route::get("device/detail/{id}", 'OtherController@deviceDetail');
    Route::get("device/s/{c?}/{k?}", 'OtherController@device');
    Route::get("device/status", 'OtherController@device');
    Route::post("update/device/{id}", 'OtherController@deviceUpdate');
    Route::post("device/command", 'OtherController@deviceCommand');
    Route::get("academic/course", 'AcademicController@indexCourse');
    Route::get("academic/course/s/{k?}", 'AcademicController@indexCourse');
    Route::get("academic/subject", 'AcademicController@indexSubject');
    Route::get("academic/subject/s/{k?}", 'AcademicController@indexSubject');
    Route::get("student", 'ClientController@student');
    Route::get("academic/section", 'AcademicController@indexSection');
    Route::get("content", 'ContentController@index');
    Route::get("content/s/{k?}", 'ContentController@index');
    Route::get("show/section/{id}", 'AcademicController@showSection');
    Route::get("academic/section/s/{k?}", 'ContentController@index');
    Route::get("client", 'VitalController@client');
    Route::get("student", 'ClientController@student');
    Route::get("session/student/{id_session}/{id_subject}", 'AcademicController@indexSessionStudent');
    Route::get("staff", 'ClientController@staff');
    Route::get("parent", 'ClientController@parent');
    Route::get("session", 'AcademicController@indexSession');
    Route::get("session/set-default/{id}", 'AcademicController@setDefaultSession');
    Route::get("session/subject/{id_session}", 'AcademicController@indexSessionSubject');
    Route::get("content/student/{id_session}/{id_subject}", 'ContentController@indexStudent');
    Route::get("content/student/detail/{id_content}", 'ContentController@showContent');
    Route::get("content/l/{subject_id}", 'ContentController@listVideo');
    Route::get("content/w/{video_id}", 'ContentController@watchVideo');
    Route::get("content/m/{subject_id}", 'ContentController@manageVideo');
    Route::get("content/sort/up/{id}", 'ContentController@sortUp');
    Route::get("content/sort/down/{id}", 'ContentController@sortDown');
    Route::get("content/mandatory/add/{id}", 'ContentController@addMandatory');
    Route::get("content/mandatory/remove/{id}", 'ContentController@removeMandatory');
    Route::get("content/detail/{content_id}", 'ContentController@detailContent');
    Route::post("content/upload", 'ContentController@storeContent');
    Route::get("quiz/list", 'AcademicController@indexQuiz');
    Route::get("quiz/question", 'AcademicController@indexQuestion');

    //route umum untuk mendapatkan data untuk grid
    Route::get('data', 'JsonController@json');

    //route untuk ke form create mana-mana object
    Route::group(array("prefix" => "create"), function () {
        Route::get("client", 'VitalController@createClient');
        Route::get("student", 'ClientController@createStudent');
        Route::get("staff", 'ClientController@createStaff');
        Route::get("academic/section", 'AcademicController@createSection');
        Route::get("academic/section/students", 'AcademicController@jsonListStudent');
        Route::get("quiz/question/{type}", 'AcademicController@createQuestion');
        Route::get("quiz", 'AcademicController@createQuiz');
    });

    //route umum untuk simpan mana-mana object yang diisi pada form
    Route::group(array("prefix" => "store"), function () {
        Route::post("option", 'OtherController@storeOption');
        Route::post("client", 'VitalController@storeClient');
        Route::post("device", 'VitalController@storeDevice');
        Route::post("student", 'ClientController@storeStudent');
        Route::post("staff", 'ClientController@storeStaff');
        Route::post("session", 'AcademicController@storeSession');
        Route::post("session/subject", 'AcademicController@storeSessionSubject');
        Route::post("session/student", 'AcademicController@storeSessionStudent');
        Route::post("course", 'AcademicController@storeCourse');
        Route::post("subject", 'AcademicController@storeSubject');
        Route::post("section", 'AcademicController@storeSection');
        Route::post("log/w", 'ContentController@storeLog');
        Route::post("content/upload", 'ContentController@storeContent');
        Route::post("academic/section/student", 'AcademicController@jsonStudent');
        Route::post("quiz/question/{type}", 'AcademicController@storeQuestion');
    });

    //route umum untuk ke form edit mana-mana page
    Route::group(array("prefix" => "edit"), function () {
        Route::get("option/{id}", 'OtherController@editOption');
        Route::get("client/{id}", 'VitalController@editClient');
        Route::get("device/{id}", 'VitalController@editDevice');
        Route::get("student/{id}", 'ClientController@editStudent');
        Route::get("staff/{id}", 'ClientController@editStaff');
        Route::get("session/{id}", 'AcademicController@editSession');
        Route::get("course/{id}", 'AcademicController@editCourse');
        Route::get("subject/{id}", 'AcademicController@editSubject');
        Route::get("academic/section/{id}", 'AcademicController@createSection');
        Route::get("student/{id}", 'ClientController@editStudent');
        Route::get("staff/{id}", 'ClientController@editStaff');
        Route::get("session/{id}", 'AcademicController@editSession');
        Route::get("course/{id}", 'AcademicController@editCourse');
        Route::get("subject/{id}", 'AcademicController@editSubject');
    });

    //route umum untuk simpan mana-mana object yang telah di edit pada form
    Route::group(array("prefix" => "update"), function () {
        Route::post("option/{id}", 'OtherController@updateOption');
        Route::post("client/{id}", 'VitalController@updateClient');
        Route::post("student/{id}", 'ClientController@updateStudent');
        Route::post("staff/{id}", 'ClientController@updateStaff');
        Route::post("session/{id}", 'AcademicController@updateSession');
        Route::post("course/{id}", 'AcademicController@updateCourse');
        Route::post("subject/{id}", 'AcademicController@updateSubject');
        Route::post("config", 'OtherController@updateConfig');
    });

    //route untuk paparkan mana-mana object
    Route::group(array("prefix" => "show"), function () {
        Route::get("client/{id}", 'VitalController@showClient');
        Route::get("student/{id}", 'ClientController@showStudent');
        Route::get("staff/{id}", 'ClientController@showStaff');
    });

    //route umum untuk delete mana-mana object
    Route::group(array("prefix" => "delete"), function () {
        Route::delete("option/{id}", 'OtherController@deleteOption');
        Route::delete("client/{id}", 'VitalController@deleteClient');
        Route::delete("device/{id}", 'VitalController@deleteDevice');
        Route::delete("student/{id}", 'ClientController@showStudent');
        Route::delete("staff/{id}", 'ClientController@deleteStaff');
        Route::delete("session/{id}", 'AcademicController@deleteSession');
        Route::delete("course/{id}", 'AcademicController@deleteCourse');
        Route::delete("subject/{id}", 'AcademicController@deleteSubject');
        Route::delete("quiz/question/{id}", 'AcademicController@deleteQuestion');
        Route::delete("session/subject/{id_session}/{id_subject}", 'AcademicController@deleteSessionSubject');
        Route::delete("session/student/{id_session}/{id_subject}/{id_student}", 'AcademicController@deleteSessionStudent');
    });

    Route::get('modal/{id}/{log}', function ($id, $log) {

        return View::make('modal_iframe');
    });

    Route::get('v/{id}/{log?}', function ($id, $log) {
        $video    = App\Models\FContentVideo::find($id);
        $section  = App\Models\ESection::where('organization_id', \Auth::user()->organization()->id)
                                       ->where('subject_id', $video->subject_id)
                                       ->where('session_id', \Session::get('session_default'))
                                       ->first();
        $end_time = 0;
        if ($log == 'yes') {
            $history  = App\Models\FLessonHistory::where('video_id', $id)->where('student_id', \Auth::user()->user_id)->where('section_id',
                $section->id)->first();
            $end_time = $history == null ? 0 : $history->end_minute;
        }

        $data = array(
            'video'    => $video,
            'log'      => $log,
            'end_time' => $end_time
        );

        return View::make('video_iframe', $data);
    });

    //route documentation
    Route::get("doc", function () {
        if (Auth::user()->role == 'Vital') {
            return View::make('doc.vital', array('title' => 'User Manual', 'breadcrumb' => 'Vital'));
        } elseif (Auth::user()->role == 'Super Admin') {
            return View::make('doc.super_admin', array('title' => 'User Manual', 'breadcrumb' => 'Vital'));
        }
        if (Auth::user()->role == 'Teacher') {
            return View::make('doc.teacher', array('title' => 'User Manual', 'breadcrumb' => 'Vital'));
        }
        if (Auth::user()->role == 'Student') {
            return View::make('doc.student', array('title' => 'User Manual', 'breadcrumb' => 'Vital'));
        }
    });

    //route faq
    Route::get("faq", function () { return View::make('faq', array('title' => 'FAQ', 'breadcrumb' => 'Frequently Asked Questions')); });

    //route about us
    Route::get("about-us", function () { return View::make('about_us', array('title' => 'About Us', 'breadcrumb' => 'Vital')); });
});

Route::group(['prefix' => 'api/v1/'], function () {
    Route::post('authenticate', 'RestController@authenticate');
    Route::get('ping', 'RestController@ping');
    Route::get('command', 'RestController@command');
    Route::get('activate', 'RestController@activate');
    Route::get('validate-token', 'RestController@validateToken');
    Route::get('get-user', 'RestController@getAuthenticatedUser');
    Route::get('get-data', 'RestController@getAuthenticatedUser');
    Route::put('synchronize', 'RestController@synchronize');
});

Route::get("s3", 'ContentController@s3One');

//http://stackoverflow.com/questions/30682421/how-to-protect-image-from-public-view-in-laravel-5
//this should be protect the assets from unauthorized access, but facing a problem with video stream, video cannot seek or cannot be played with this method.
//need more time. so rolling back using public folder as asset location 
/*Route::get('assets/image/{image?}', 'ContentController@image');
Route::get('assets/file/{file?}', 'ContentController@file');
Route::get('assets/thumbnail/{image?}', 'ContentController@thumbnail');*/

Route::get('mem', function () {
    //$id3 = new getID3();
    //dd($id3->analyze(public_path('e807a2e6112042f19a85f0855612587f.mp4')));

    /*

    echo exec('~/b.sh');*/
});
Route::get('logout', 'LoginController@logout');

