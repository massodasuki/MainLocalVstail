<?php
namespace App\Http\Controllers;

use App\Libs\Vital;
use App\Models;
use App\Repositories\AcademicRepository as Academic;
use App\Repositories\ClientRepository as Client;

class AcademicController extends Controller
{

    use Vital;
    protected $academic;

    public function __construct(Academic $academic, Client $client)
    {
        $this->academic = $academic;
        $this->client   = $client;
        \App::setLocale(\Session::get('locale'));
    }

    public function indexSession()
    {
        $data = array(
            'title'      => 'Sessions',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Sessions</strong></li>',
            'sessions'   => $this->academic->getSessions()->get()
        );

        return \View::make('organization.academic.session', $data);
    }

    public function storeSession()
    {
        $rules     = [
            'session' => 'required'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $session = $this->academic->storeSession(\Input::all());
            if ($session['save_status']) {
                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'id'       => $session['session']['id'],
                        'session'  => $session['session']['session'],
                        'semester' => $session['session']['semester'],
                        'start_at' => date('d M Y', strtotime($session['session']['start_dt'])),
                        'end_at'   => date('d M Y', strtotime($session['session']['end_dt']))
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function editSession($id)
    {
        $session = $this->academic->getSession($id);
        $session = array(
            'id'       => $session->id,
            'session'  => $session->session,
            'semester' => $session->semester,
            'start_dt' => date('d-m-Y', strtotime($session->start_dt)),
            'end_dt'   => date('d-m-Y', strtotime($session->end_dt))
        );

        return \Response::json($session);
    }

    public function updateSession($id)
    {
        $rules     = [
            'session' => 'required'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $session = $this->academic->updateSession(\Input::all(), $id);
            if ($session['save_status']) {
                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'id'       => $session['session']['id'],
                        'session'  => $session['session']['session'],
                        'semester' => $session['session']['semester'],
                        'start_at' => date('d M Y', strtotime($session['session']['start_dt'])),
                        'end_at'   => date('d M Y', strtotime($session['session']['end_dt']))
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function deleteSession($id)
    {
        $delete_status = $this->academic->deleteSession($id);

        return \Response::json($delete_status);
    }

    public function setDefaultSession($id)
    {
        $this->academic->setDefaultSession($id);

        return \Redirect::to('session');
    }

    public function indexSessionSubject($id_session)
    {
        $subjects = array();

        foreach ($this->academic->getSubjects()->get() as $subject) {
            $subjects[$subject->id] = $subject->subject_name;
        }

        $data = array(
            'title'      => 'Manage Subject',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li>
                             <li><a href="' . \URL::to('session') . '">Sessions</a></li>
                             <li class="active"><strong>Subjects</strong></li>',
            'session'    => $this->academic->getSession($id_session),
            'subjects'   => $subjects
        );

        return \View::make('organization.academic.session_subject', $data);
    }

    public function storeSessionSubject()
    {
        $rules     = [
            'subject' => 'required'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $subject = $this->academic->storeSessionSubject(\Input::all());

            return \Response::json(array('is_error' => false, 'msg' => $subject));
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function deleteSessionSubject($id_session, $id_subject)
    {
        $delete_status = $this->academic->deleteSessionSubject($id_session, $id_subject);

        return \Response::json($delete_status);
    }

    public function indexSessionStudent($id_session, $id_subject)
    {
        if (\Auth::user()->role == 'Super Admin') {
            $students = array();

            foreach ($this->client->getAllStudents()->get() as $student) {
                $students[$student->id] = $student->student_name;
            }

            /*$current_student = $this->academic->getSession($id_session)->subjects()->where('subject_id', $id_subject)
                                              ->get()->first()->pivot->student_id;*/

            $data = array(
                'title'        => 'Manage Student',
                'breadcrumb'   => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li>
                             <li><a href="' . \URL::to('session') . '">Sessions</a></li>
                             <li><a href="' . \URL::to('session/subject/' . \Request::segment(3)) . '">Subjects</a></li>
                             <li class="active"><strong>Students</strong></li>',
                //'ses_sub_student' => $this->client->getStudentsIn(json_decode($current_student)),
                'session_name' => $this->academic->getSession(\Request::segment(3))->session,
                'subject_name' => $this->academic->getSubject(\Request::segment(4))->subject_name,
                'students'     => $students
            );

            return \View::make('organization.academic.session_student', $data);
        }
        if (\Auth::user()->role == 'Teacher') {
            $current_student = $this->academic->getSession($id_session)->subjects()->where('subject_id', $id_subject)
                                              ->get()->first()->pivot->student_id;
            $data            = array(
                'title'           => 'Manage Student',
                'breadcrumb'      => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li>
                                         <li><a href="' . \URL::to('session') . '">Sessions</a></li>
                                         <li><a href="' . \URL::to('session/subject/' . \Request::segment(3)) . '">Subjects</a></li>
                                         <li class="active"><strong>Students</strong></li>',
                'ses_sub_student' => $this->client->getStudentsIn(json_decode($current_student)),
                'session_name'    => $this->academic->getSession(\Request::segment(3))->session,
                'subject_name'    => $this->academic->getSubject(\Request::segment(4))->subject_name
            );

            return \View::make('teacher.student', $data);
        }
    }

    public function storeSessionStudent()
    {
        $rules     = [
            'student' => 'required'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $student = $this->academic->storeSessionStudent(\Input::all());

            return \Response::json(array('is_error' => false, 'msg' => $student));
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function deleteSessionStudent($id_session, $id_subject, $id_student)
    {
        $delete_status = $this->academic->deleteSessionStudent($id_session, $id_subject, $id_student);

        return \Response::json($delete_status);
    }

    public function indexCourse()
    {
        $data = array(
            'title'      => 'Courses',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Courses</strong></li>',
            'courses'    => $this->academic->getCourses()->paginate(\Config::get('vital.option_paginate'))
        );

        if (\Auth::user()->role == 'Super Admin') {
            return \View::make('organization.academic.course', $data);
        } elseif (\Auth::user()->role == 'Teacher') {
            return \View::make('teacher.academic.course', $data);
        }
    }

    public function storeCourse()
    {
        $rules     = [
            'course_name' => 'required'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $course = $this->academic->storeCourse(\Input::all());
            if ($course['save_status']) {
                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'id'          => $course['course']['id'],
                        'course_name' => $course['course']['course_name'],
                        'created_at'  => date('d M Y', strtotime($course['course']['created_at']))
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function editCourse($id)
    {
        $course = $this->academic->getCourse($id);
        $course = array(
            'id'          => $course->id,
            'course_name' => $course->course_name,
            'code'        => $course->code,
            'description' => $course->description
        );

        return \Response::json($course);
    }

    public function updateCourse($id)
    {
        $rules     = [
            'course_name' => 'required'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $course = $this->academic->updateCourse(\Input::all(), $id);
            if ($course['save_status']) {
                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'id'          => $course['course']['id'],
                        'course_name' => $course['course']['course_name'],
                        'created_at'  => date('d M Y', strtotime($course['course']['created_at']))
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function deleteCourse($id)
    {
        $delete_status = $this->academic->deleteCourse($id);

        return \Response::json($delete_status);
    }

    public function indexSubject()
    {
        $data = array(
            'title'      => 'Subjects',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Subjects</strong></li>'
        );
        if (\Auth::user()->role == 'Super Admin') {
            $data['courses']   = $this->academic->getCourses()->lists('course_name', 'id')->toArray();
            $data['lecturers'] = $this->academic->getLecturers()->lists('staff_name', 'id')->toArray();
            $data['subjects']  = $this->academic->getSubjects()->paginate(\Config::get('vital.option_paginate'));

            return \View::make('organization.academic.subject', $data);
        } elseif (\Auth::user()->role == 'Teacher') {
            $section    = Models\ESection::where('session_id', \Session::get('session_default'))->where('lecturer_id', 'like',
                '%' . \Auth::user()->user_id . '%')->get();
            $subject_id = array();
            foreach ($section as $sec) {
                array_push($subject_id, $sec->subject_id);
            }

            if (\Request::is('*academic/subject/s/*')) {
                if (\Request::segment(4) == null) {
                    \Redirect::to('subject');
                }

                $data['subjects'] = Models\ESubject::whereIn('id', $subject_id)
                                                   ->where('subject_name', 'like', '%' . \Request::segment(4) . '%')
                                                   ->orWhere('description', 'like', '%' . \Request::segment(4) . '%')
                                                   ->orWhere('code', 'like', '%' . \Request::segment(4) . '%')
                                                   ->orderBy('created_at', 'DESC')->paginate(\Config::get('vital.option_paginate'));
            } else {
                $data['subjects'] = Models\ESubject::whereIn('id', $subject_id)->orderBy('created_at',
                    'DESC')->paginate(\Config::get('vital.option_paginate'));
            }

            return \View::make('teacher.academic.subject', $data);
        } else {
            return \Redirect::to('/');
        }
    }

    public function storeSubject()
    {
        $rules     = [
            'subject_name' => 'required',
            'course_id'    => 'required',
            'lecturer_id'  => 'required'
        ];
        $att       = [
            'course_id'   => 'course',
            'lecturer_id' => 'lecture'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages, $att);
        if (!$validator->fails()) {
            $subject = $this->academic->storeSubject(\Input::all());
            if ($subject['save_status']) {
                $lecturers = null;
                $courses   = null;
                $i         = 0;
                foreach ($subject['subject']->lecturers()->get() as $lecturer) {
                    $i++;
                    $lecturers .= '<em style="font-style: normal; text-decoration: underline;">' . $lecturer->staff_name . '</em>';
                    if ($i != $subject['subject']->lecturers()->count()) {
                        $lecturers .= ' , ';
                    }
                }
                $i = 0;
                foreach ($subject['subject']->courses()->get() as $course) {
                    $i++;
                    $courses .= '<em style="font-style: normal; text-decoration: underline;">' . $course->course_name . '</em>';
                    if ($i != $subject['subject']->courses()->count()) {
                        $courses .= ' , ';
                    }
                }

                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'id'           => $subject['subject']['id'],
                        'description'  => $subject['subject']['description'],
                        'subject_name' => $subject['subject']['code'] != null ? $subject['subject']['code'] . ' - ' . $subject['subject']['subject_name'] : $subject['subject']['subject_name'],
                        'lecturers'    => $lecturers,
                        'courses'      => $courses
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function editSubject($id)
    {
        $data                = $this->academic->getSubject($id)->toArray();
        $data['id']          = $data['id'];
        $data                = array_except($data, array('organization_id'));
        $data['course_id']   = json_decode($data['course_id']);
        $data['lecturer_id'] = json_decode($data['lecturer_id']);

        return \Response::json($data);
    }

    public function updateSubject($id)
    {
        $rules     = [
            'subject_name' => 'required',
            'course_id'    => 'required',
            'lecturer_id'  => 'required'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $subject = $this->academic->updateSubject(\Input::all(), $id);
            if ($subject['save_status']) {
                $lecturers = null;
                $courses   = null;
                $i         = 0;
                foreach ($subject['subject']->lecturers()->get() as $lecturer) {
                    $i++;
                    $lecturers .= '<em style="font-style: normal; text-decoration: underline;">' . $lecturer->staff_name . '</em>';
                    if ($i != $subject['subject']->lecturers()->count()) {
                        $lecturers .= ' , ';
                    }
                }
                $i = 0;
                foreach ($subject['subject']->courses()->get() as $course) {
                    $i++;
                    $courses .= '<em style="font-style: normal; text-decoration: underline;">' . $course->course_name . '</em>';
                    if ($i != $subject['subject']->courses()->count()) {
                        $courses .= ' , ';
                    }
                }

                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'id'           => $subject['subject']['id'],
                        'description'  => $subject['subject']['description'],
                        'subject_name' => $subject['subject']['code'] != null ? $subject['subject']['code'] . ' - ' . $subject['subject']['subject_name'] : $subject['subject']['subject_name'],
                        'lecturers'    => $lecturers,
                        'courses'      => $courses
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function deleteSubject($id)
    {
        $delete_status = $this->academic->deleteSubject($id);

        return \Response::json($delete_status);
    }

    public function indexSection()
    {
        $data = array(
            'title'      => 'Sections',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Sections</strong></li>'
        );
        if (\Auth::user()->role == 'Super Admin') {
            $data['sections'] = $this->academic->getSections()->paginate(\Config::get('vital.option_paginate'));

            return \View::make('organization.academic.section.index', $data);
        } elseif (\Auth::user()->role == 'Teacher') {
            $data['sections'] = Models\ESection::where('session_id', \Session::get('session_default'))
                                               ->where('lecturer_id', 'like', '%' . \Auth::user()->user_id . '%')
                                               ->paginate(\Config::get('vital.option_paginate'));

            return \View::make('teacher.academic.section', $data);
        } else {
            return \Redirect::to('/');
        }
    }

    public function createSection()
    {
        $data = array(
            'title'      => 'Create Section',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('academic/section') . '">Create Section</a></li>
                             <li class="active"><strong>Section Form</strong></li>',
            'sessions'   => array('' => '') + $this->academic->getSessions()->lists('session', 'id')->toArray(),
            'subjects'   => array('' => '') + $this->academic->getSubjects()->lists('subject_name', 'id')->toArray(),
            'lecturers'  => $this->academic->getLecturers()->lists('staff_name', 'id')->toArray()
        );

        return \View::make('organization.academic.section.form', $data);
    }

    public function jsonListStudent()
    {
        return \Response::json($this->client->jsonListStudent()->get());
    }

    public function jsonStudent()
    {
        $existing = json_decode(\Input::get('all_student_id')) == null ? array() : json_decode(\Input::get('all_student_id'));
        if (in_array(\Input::get('student_id'), $existing)) {
            return \Response::json(array('is_error' => true, 'error' => 'Studen already exists in this section!'));
        } else {
            $student = $this->client->getStudent(\Input::get('student_id'));
            array_push($existing, $student->id);
            $student->existing = json_encode($existing);
            $avatar            = $student->avatar == null ? \URL::to('assets/images/no_picture.png') : \URL::to('assets/images/' . $this->getFile($student->avatar));

            return \Response::json(array('is_error' => false, 'student' => $student, 'avatar' => $avatar));
        }
    }

    public function storeSection()
    {
        $rules     = [
            'section'     => 'required',
            'session_id'  => 'required',
            'subject_id'  => 'required',
            'lecturer_id' => 'required'
        ];
        $att       = [
            'session_id'  => 'session',
            'subject_id'  => 'subject',
            'lecturer_id' => 'lecturer'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages, $att);
        if (!$validator->fails()) {
            $this->academic->storeSection(\Input::all());
        } else {
            return \Redirect::to('create/academic/section')
                            ->withInput()
                            ->withErrors($validator);
        }

        return \Redirect::to('academic/section');
    }

    public function showSection($id)
    {
        $section = $this->academic->getSection($id);
        $data    = array(
            'title'      => 'Show Section',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>Section Detail</strong></li>',
            'section'    => $section
        );

        return \View::make('organization.academic.section.show', $data);
    }

    public function editSection()
    {
        $data = array(
            'title'      => 'Edit Section',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('academic/section') . '">Section</a></li>
                             <li class="active"><strong>Edit</strong></li>',
            'sessions'   => array('' => '') + $this->academic->getSessions()->lists('session', 'id')->toArray(),
            'subjects'   => array('' => '') + $this->academic->getSubjects()->lists('subject_name', 'id')->toArray(),
            'lecturers'  => $this->academic->getLecturers()->lists('staff_name', 'id')->toArray()
        );

        return \View::make('organization.academic.section.form', $data);
    }

    public function indexQuiz()
    {
        $data = array(
            'title'      => 'Quiz',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Quiz</strong></li>'
        );
        if (\Auth::user()->role == 'Teacher') {
            $data['quizs'] = Models\GQuiz::leftJoin('e_section', 'g_quiz.section_id', '=', 'e_section.id')
                                         ->where('e_section.session_id', \Session::get('session_default'))
                                         ->where('e_section.lecturer_id', 'like', '%' . \Auth::user()->user_id . '%')
                                         ->paginate(\Config::get('vital.option_paginate'));

            return \View::make('teacher.quiz.quiz', $data);
        } else {
            return \Redirect::to('/');
        }
    }

    public function indexQuestion()
    {
        $data = array(
            'title'      => 'Question',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Question</strong></li>',
            'questions'  => $this->academic->getQuestions()->paginate(\Config::get('vital.option_paginate'))
        );

        return \View::make('teacher.quiz.question', $data);
    }

    public function createQuestion($type)
    {

        $data = array(
            'title'      => 'New Question',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li>
                             <li><a href="' . \URL::to('quiz/question') . '">Question</a></li>
                             <li><strong>New Question</strong></li>'
        );

        $view = 'single_choice';
        if ($type == 2) {
            $view = 'multiple_choice';
        } elseif ($type == 3) {
            $view = 'essay';
        } elseif ($type == 4) {
            $view = 'true_false';
        } elseif ($type == 5) {
            $view = 'short_answer';
        }

        return \View::make('teacher.quiz.question.' . $view, $data);
    }

    public function storeQuestion($type)
    {
        $view = 'single_choice';
        if ($type == 2) {
            $view = 'multiple_choice';
        } elseif ($type == 3) {
            $view = 'essay';
        } elseif ($type == 4) {
            $view = 'true_false';
        } elseif ($type == 5) {
            $view = 'short_answer';
        }

        $this->academic->storeQuestion(\Input::all(), $type);

        return \Redirect::to('quiz/question');
    }

    public function deleteQuestion($id)
    {
        $delete_status = $this->academic->deleteQuestion($id);

        return \Response::json($delete_status);
    }

    public function createQuiz()
    {
        $data = array(
            'title'      => 'Create Quiz',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('academic/section') . '">Create Quiz</a></li>
                             <li class="active"><strong>Quiz Form</strong></li>',
            'questions'   => array('' => '') + Models\GQuestion::where('created_by', \Auth::user()->user_id)->lists('title', 'id')->toArray()
        );

        return \View::make('teacher.quiz.form', $data);
    }
}
