<?php
namespace App\Http\Controllers;

use App\Libs\Vital;
use App\Models;
use App\Repositories\JsonRepository as Json;

/*
 * <br />55a0c60437dae<br />55a0c60437dfb<br />55a0c60437e48<br />55a0c60437e96<br />55a0c60437ee3<br />55a0c60437f30<br />55a0c60437f7d<br />55a0c60437fca<br />55a0c60438017<br />55a0c60438064<br />55a0c604380b1<br />55a0c604380fe<br />55a0c60438163<br />55a0c604381b5<br />55a0c60438203<br />55a0c6043824f<br />55a0c6043829c<br />55a0c604382e9<br />55a0c60438336<br />55a0c60438382<br />55a0c604383cf<br />55a0c6043841b<br />55a0c60438467<br />55a0c604384b3<br />55a0c6043850b<br />55a0c60438557<br />55a0c604385a3<br />55a0c604385f0<br />55a0c6043863c<br />55a0c60438688<br />55a0c604386d5<br />55a0c6043872a<br />55a0c60438777<br />55a0c604387c3<br />55a0c6043880f<br /><br /><br />"
 *
 */

class JsonController extends Controller
{

    use Vital;

    protected $json;

    public function __construct(Json $json)
    {
        $this->json = $json;
    }

    public function json()
    {
        $section = \Request::get('b');
        switch ($section) {
            case '55a0c60437d14': { //Grid untuk option
                return \Response::json($this->json->optionGrid(), 200, [], JSON_NUMERIC_CHECK);
                break;
            }
            case '55a0c60437b36': { //Grid untuk senarai client
                return \Response::json($this->json->clientGrid(), 200, [], JSON_NUMERIC_CHECK);
                break;
            }
            case '55a0c60437bd8': { //Grid untuk senarai staff
                return \Response::json($this->json->staffGrid(), 200, [], JSON_NUMERIC_CHECK);
                break;
            }
            case '55a0c60437c27': { //Grid untuk senarai student
                return \Response::json($this->json->studentGrid(), 200, [], JSON_NUMERIC_CHECK);
                break;
            }
            case '55a0c60437c77': { //Grid untuk senarai subject
                return \Response::json($this->json->subjectGrid(), 200, [], JSON_NUMERIC_CHECK);
                break;
            }
            case '55a0c604388a8': { //Get json ping data
                return \Response::json($this->json->pingLog(), 200, [], JSON_NUMERIC_CHECK);
                break;
            }
            case '55a0c6043885c': { //Get json ping data
                return \Response::json($this->json->pingGrid(), 200, [], JSON_NUMERIC_CHECK);
                break;
            }
        }
    }
}
