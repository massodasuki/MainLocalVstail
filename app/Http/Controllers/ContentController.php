<?php
namespace App\Http\Controllers;

use App\Libs\Vital;
use App\Models;
use App\Models\AAssets;
use App\Repositories\ContentRepository as Content;
use Aws\CommandInterface;
use Aws\Exception\MultipartUploadException;
use Aws\S3\MultipartUploader;
use Config;
use finfo;
use getID3;
use Ramsey\Uuid\Uuid;
use Storage;

class ContentController extends Controller
{

    use Vital;

    public function __construct(Content $content)
    {
        $this->content = $content;
        \App::setLocale(\Session::get('locale'));
    }

    public function index()
    {
        $data = array(
            'title'      => 'Choose A Subject',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Subject</strong></li>'
        );

        if (\Auth::user()->role == 'Teacher') {
            $section    = Models\ESection::where('session_id', \Session::get('session_default'))->where('lecturer_id', 'like',
                '%' . \Auth::user()->user_id . '%')->get();
            $subject_id = array();
            foreach ($section as $sec) {
                array_push($subject_id, $sec->subject_id);
            }

            if (\Request::is('*content/s/*')) {
                if (\Request::segment(3) == null) {
                    \Redirect::to('content');
                }

                $data['subjects'] = Models\ESubject::whereIn('id', $subject_id)
                                                   ->where('subject_name', 'like', '%' . \Request::segment(3) . '%')
                                                   ->orWhere('description', 'like', '%' . \Request::segment(3) . '%')
                                                   ->orWhere('code', 'like', '%' . \Request::segment(3) . '%')
                                                   ->orderBy('created_at', 'DESC')->paginate(\Config::get('vital.option_paginate'));
            } else {
                $data['subjects'] = Models\ESubject::whereIn('id', $subject_id)->orderBy('created_at',
                    'DESC')->paginate(\Config::get('vital.option_paginate'));
            }

            return \View::make('teacher.content.index', $data);
        } elseif (\Auth::user()->role == 'Student') {
            $section    = Models\ESection::where('session_id', \Session::get('session_default'))->where('student_id', 'like',
                '%' . \Auth::user()->user_id . '%')->get();
            $subject_id = array();
            foreach ($section as $sec) {
                array_push($subject_id, $sec->subject_id);
            }

            if (\Request::is('*content/s/*')) {
                if (\Request::segment(3) == null) {
                    \Redirect::to('content');
                }

                $data['subjects'] = Models\ESubject::whereIn('id', $subject_id)
                                                   ->where('subject_name', 'like', '%' . \Request::segment(3) . '%')
                                                   ->orWhere('description', 'like', '%' . \Request::segment(3) . '%')
                                                   ->orWhere('code', 'like', '%' . \Request::segment(3) . '%')
                                                   ->orderBy('created_at', 'DESC')->paginate(\Config::get('vital.option_paginate'));
            } else {
                $data['subjects'] = Models\ESubject::whereIn('id', $subject_id)->orderBy('created_at',
                    'DESC')->paginate(\Config::get('vital.option_paginate'));
            }

            return \View::make('student.content.index', $data);
        }
    }

    public function manageVideo($subject_id)
    {
        $data = array(
            'title'      => 'Manage Content',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Manage Content</strong></li>',
            'contents'   => Models\FContentVideo::orderBy('arrangement')->get(),
            'subject'    => Models\ESubject::find($subject_id)
        );

        return \View::make('teacher.content.content', $data);
    }

    public function detailContent($content_id)
    {
        $data = array(
            'title'      => 'Content Detail',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Content Detail</strong></li>',
            'video'      => Models\FContentVideo::find($content_id),
            'log'        => Models\FLessonHistory::where('video_id', $content_id)->get()
        );

        return \View::make('teacher.content.detail', $data);
    }


    /**
     * Status asset video mestilah dalam :
     * Content Video Original - Original file yang diupload, file format mestilah samada dalam mp4, flv, webm, wmv, atau mov
     * Content Video MP4 - Jika video yg diupload berbentuk MP4, atau jika video yang telah diencode kepada MP4
     * Content Video WEBM - Jika video yg diupload berbentuk WEBM, atau jika video yang telah diencode kepada WEBM
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeContent()
    {
        $getID3        = new getID3;
        $upload        = $this->multipartPlupload();
        $videoPath     = env('ASSETS_STORAGE') . 'contents' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . \Auth::user()->login->id;
        
        if (!is_dir($videoPath)) {
            mkdir($videoPath);
        }
        
        $thumbnailPath = env('ASSETS_STORAGE') . 'contents' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR;
        
        if ($upload[0] == 1) {
            $file = new \SplFileInfo(env('ASSETS_STORAGE') . 'temp' . DIRECTORY_SEPARATOR . $upload[1]);
            if (\Input::get('type') == 'video') {
                $asset_id = Uuid::uuid4()->getHex();
                $videoId  = Uuid::uuid4()->getHex();
                $new_name = $asset_id . '.' . $file->getExtension();

                copy(env('ASSETS_STORAGE') . 'temp' . DIRECTORY_SEPARATOR . $file->getFilename(), $videoPath . DIRECTORY_SEPARATOR . $new_name);

                $asset             = new AAssets();
                $asset->id         = $asset_id;
                $asset->for        = 'Content Video Original';
                $asset->for_id     = $videoId;
                $asset->upload_by     = \Auth::user()->login->id;
                $asset->asset_name = $file->getFilename();
                $asset->file_size  = $file->getSize();
                $asset->md5        = md5_file($videoPath . DIRECTORY_SEPARATOR . $new_name);
                $asset->save();

                Storage::disk('assets')->delete('temp' . DIRECTORY_SEPARATOR . $file->getFilename());

                $id3                = $getID3->analyze($videoPath . DIRECTORY_SEPARATOR . $new_name);
                $video              = new Models\FContentVideo();
                $video->id          = $videoId;
                $video->subject_id  = \Input::get('subject_id');
                $video->title       = \Input::get('title');
                $video->description = \Input::get('description');
                $video->resolution  = $id3['video']['resolution_x'] . " x " . $id3['video']['resolution_y'];
                $video->duration    = $id3['playtime_string'];

                //$queue = new Models\FProcessQueue();
                if ($file->getExtension() == 'mp4') {
                    $video->mp4 = $asset_id;

                    /*$queue->id = Uuid::uuid4()->getHex();
                    $queue->asset_id = $asset_id;
                    $queue->process = 'encode webm';
                    $queue->save();*/
                }
                if ($file->getExtension() == 'webm') {
                    $video->webm = $asset_id;

                    /*$queue->id = Uuid::uuid4()->getHex();
                    $queue->asset_id = $asset_id;
                    $queue->process = 'encode mp4';
                    $queue->save();*/
                }
                
                /*if ($file->getExtension() != 'webm' && $file->getExtension() != 'mp4') {
                    $queue->id = Uuid::uuid4()->getHex();
                    $queue->asset_id = $asset_id;
                    $queue->process = 'encode webm';
                    $queue->save();

                    $queue->id = Uuid::uuid4()->getHex();
                    $queue->asset_id = $asset_id;
                    $queue->process = 'encode mp4';
                    $queue->save();
                }*/

                $video->save();

                return \Response::json($videoId);
            } else {
                $thumbnail_id = Uuid::uuid4()->getHex();
                $new_name     = $thumbnail_id . '.' . $file->getExtension();
                Storage::disk('assets')->copy('temp' . DIRECTORY_SEPARATOR . $file->getFilename(),
                    'contents' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . $new_name);
                $asset             = new AAssets();
                $asset->id         = $thumbnail_id;
                $asset->for        = 'Content Thumbnail';
                $asset->for_id     = \Input::get('video_id');
                $asset->upload_by     = \Auth::user()->login->id;
                $asset->asset_name = $file->getFilename();
                $asset->file_size  = $file->getSize();
                $asset->md5        = md5_file($thumbnailPath . $new_name);
                $asset->save();
                Storage::disk('assets')->delete('temp' . DIRECTORY_SEPARATOR . $file->getFilename());

                return \Response::json('success');
            }
        }
    }

    public function deleteContent($id)
    {
        Models\FContentVideo::find($id)->delete();

        return \Response::json(true);
    }

    public function listVideo()
    {
        $data = array(
            'title'      => 'Show Content',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>Show Content</strong></li>',
            'contents'   => Models\FContentVideo::all()
        );

        return \View::make('student.content.content', $data);
    }

    public function watchVideo($video_id)
    {
        $content = Models\FContentVideo::find($video_id);
        $section = Models\ESection::where('organization_id', \Auth::user()->organization()->id)
                                  ->where('subject_id', $content->subject_id)
                                  ->where('session_id', \Session::get('session_default'))
                                  ->first();
        $data    = array(
            'title'      => $content->title,
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">'. trans('dashboard.menu') .'</a></li><li class="active"><strong>' . $content->title . '</strong></li>',
            'video'      => $content,
            'history'    => Models\FLessonHistory::where('video_id', $video_id)->where('student_id', \Auth::user()->user_id)->where('section_id',
                $section->id)->first()
        );

        return \View::make('student.content.watch', $data);
    }

    public function storeLog()
    {
        if (\Auth::user()->role == 'Student') {
            $content       = Models\FContentVideo::find(\Input::get('content_id'));
            $section       = Models\ESection::where('organization_id', \Auth::user()->organization()->id)
                                            ->where('subject_id', $content->subject_id)
                                            ->where('session_id', \Session::get('session_default'))
                                            ->first();
            $already_watch = Models\FLessonHistory::where('student_id', \Auth::user()->user_id)
                                                  ->where('video_id', $content->id)
                                                  ->where('section_id', $section->id)
                                                  ->first();
            if ($already_watch == null) {
                $log             = new Models\FLessonHistory();
                $log->id         = Uuid::uuid4()->getHex();
                $log->student_id = \Auth::user()->user_id;
                $log->video_id   = $content->id;
                $log->section_id = $section->id;
                $log->end_minute = \Input::get('minute');
                $log->save();
            } else {
                $already_watch->end_minute = \Input::get('minute');
                $already_watch->save();
            }
        }
    }

    public function image($filename)
    {
        $storagePath = env('ASSETS_STORAGE') . 'images' . DIRECTORY_SEPARATOR . $filename;

        return \Image::make($storagePath)->response();
    }

    public function file($id)
    {
        $asset       = Models\AAssets::find($id);
        $storagePath = env('ASSETS_STORAGE') . 'files' . DIRECTORY_SEPARATOR . $this->getFile($asset);
        
        return \Response::make(file_get_contents($storagePath), 200, [
            'Content-Type'        =>  \File::mimeType($storagePath),
            'Content-Disposition' => 'inline; filename="' . $asset->asset_name . '"'
        ]);
    }

    public function thumbnail($filename)
    {
        $storagePath = env('ASSETS_STORAGE') . 'contents' . DIRECTORY_SEPARATOR . 'thumbnails' . DIRECTORY_SEPARATOR . $filename;

        return \Image::make($storagePath)->response();
    }

    public function s3One()
    {
        //$disk = Storage::disk('s3');
        //$disk->put('target.mp4', file_get_contents(storage_path('testvideo.mp4')));
        $disk   = Storage::disk('s3');
        $handle = fopen(storage_path('testvideo.mp4'), 'rb');
        $param  = [
            'bucket'          => Config::get('filesystems.disks.s3.bucket'),
            'key'             => '06.mp4',
            'before_initiate' => function (CommandInterface $command) {
                echo 'initiateMultipartUpload:: Starting copying of artwork';
            },
            'before_upload'   => function (CommandInterface $command) {
                echo 'initiateMultipartUpload:: Uploading chunk {' . $command["PartNumber"] . '}';
            },
            'before_complete' => function (CommandInterface $command) {
                echo 'initiateMultipartUpload:: Finishing up copying';
            },
        ];

        $uploader = new MultipartUploader($disk->getDriver()->getAdapter()->getClient(), $handle, $param);
        $count    = 1;
        // Source: http://docs.aws.amazon.com/aws-sdk-php/v3/guide/service/s3-multipart-upload.html
        do {
            try {
                // Start multipart copy of the file to the real path of the file on niiomediafiles bucket
                $response = $uploader->upload();
                \Log::info('initiateMultipartUpload:: Multipart upload started #'.$count);
            } catch (MultipartUploadException $e) {
                // Something went wrong on the # try, let's try again to continue from the same end state of the previous one
                //CakeLog::write('info', 'initiateMultipartUpload::ArtworkId '.$artworkId.' Multipart exception '.print_r($e->getMessage(), true));
                $count++;
                rewind($handle);
                $uploader = new MultipartUploader($disk->getDriver()->getAdapter()->getClient(), $handle, $param + [
                        'state' => $e->getState()
                    ]);
            }
        } while (!isset($response));

        \Log::info($response);

    }

    private function multipartPlupload()
    {
        $return = null;
        if (empty($_FILES) || $_FILES['file']['error']) {
            $return = [0, "Failed to move uploaded file."];
        }

        $chunk  = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
        $filePath = env('ASSETS_STORAGE') . 'temp' . DIRECTORY_SEPARATOR . $fileName;

        // Open temp file
        $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        if ($out) {
            // Read binary input stream and append it to temp file
            $in = @fopen($_FILES['file']['tmp_name'], "rb");

            if ($in) {
                while ($buff = fread($in, 4096)) {
                    fwrite($out, $buff);
                }
            } else {
                $return = [0, "Failed to open input stream."];
            }

            @fclose($in);
            @fclose($out);

            @unlink($_FILES['file']['tmp_name']);
        } else {
            $return = [0, "Failed to open output stream."];
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
            $return = [1, $fileName];
        } else {
            $return = [0, "Not finish upload."];
        }

        return $return;
    }

    /*
     * The purpose is to protect video from unauthorized access, but it so difficult because of the unable to produce video from outside public directory
     * The major problem is this video cannot be seeking, video not encoded properly, or video not found
     * This is because of the read byte by byte using this function
     * Meanwhile, move all video content back to the public folder until found solid solution to this
     * FYI, udemy, or laracast can view video even without login to the system.
     */
    public function mp4($id)
    {
        $asset = Models\AAssets::find($id);
        //https://blog.weckx.net/en/streaming-videos-with-php/
        //https://licson.net/post/stream-videos-php/
        $storagePath = env('ASSETS_STORAGE') . 'contents' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . $this->getFile($asset);

        // Clears the cache and prevent unwanted output
        ob_clean();
        @ini_set('error_reporting', E_ALL & ~E_NOTICE);
        @ini_set('zlib.output_compression', 'Off');

        $file = $storagePath; // The media file's location
        $mime = "video/mp4"; // The MIME type of the file, this should be replaced with your own.
        $size = filesize($file); // The size of the file

        // Send the content type header
        header('Content-type: ' . $mime);
        // Check if it's a HTTP range request
        if (isset($_SERVER['HTTP_RANGE'])) {
            // Parse the range header to get the byte offset
            $ranges = array_map(
                'intval', // Parse the parts into integer
                explode(
                    '-', // The range separator
                    substr($_SERVER['HTTP_RANGE'], 6) // Skip the `bytes=` part of the header
                )
            );

            // If the last range param is empty, it means the EOF (End of File)
            if (!$ranges[1]) {
                $ranges[1] = $size - 1;
            }

            // Send the appropriate headers
            header('HTTP/1.1 206 Partial Content');
            header('Accept-Ranges: bytes');
            header('Content-Length: ' . ($ranges[1] - $ranges[0])); // The size of the range

            // Send the ranges we offered
            header(
                sprintf(
                    'Content-Range: bytes %d-%d/%d', // The header format
                    $ranges[0], // The start range
                    $ranges[1], // The end range
                    $size // Total size of the file
                )
            );

            // It's time to output the file
            $f         = fopen($file, 'rb'); // Open the file in binary mode
            $chunkSize = 8192; // The size of each chunk to output

            // Seek to the requested start range
            fseek($f, $ranges[0]);

            // Start outputting the data
            while (true) {
                // Check if we have outputted all the data requested
                if (ftell($f) >= $ranges[1]) {
                    break;
                }

                // Output the data
                echo fread($f, $chunkSize);

                // Flush the buffer immediately
                @ob_flush();
                flush();
            }
        } else {
            // It's not a range request, output the file anyway
            header('Content-Length: ' . $size);

            // Read the file
            @readfile($file);

            // and flush the buffer
            @ob_flush();
            flush();
        }
    }

    /*
     * The purpose is to protect video from unauthorized access, but it so difficult because of the unable to produce video from outside public directory
     * The major problem is this video cannot be seeking, video not encoded properly, or video not found
     * This is because of the read byte by byte using this function
     * Meanwhile, move all video content back to the public folder until found solid solution to this
     * FYI, udemy, or laracast can view video even without login to the system.
     */
    public function webm($id)
    {
        $asset = Models\AAssets::find($id);
        //http://stackoverflow.com/questions/23014913/webm-file-not-seekable-in-chrome-when-loaded-through-php-other-browsers-work
        $path = env('ASSETS_STORAGE') . 'contents' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR . $this->getFile($asset);

        // Determine file mimetype
        $finfo  = new finfo(FILEINFO_MIME);
        $mime   = $finfo->file($path);
        $offset = 0;
        // File size
        $size   = filesize($path);
        $length = $size;

        $partialContent = false;

        ob_clean();
        // Set response content-type
        header('Content-type: ' . $mime);

        // Check if we have a Range header
        if (isset($_SERVER['HTTP_RANGE'])) {
            $partialContent = true;

            if (!preg_match('/bytes=(\d+)-(\d+)?/', $_SERVER['HTTP_RANGE'], $matches)) {
                header('HTTP/1.1 416 Requested Range Not Satisfiable');
                header('Content-Range: bytes */' . $size);
                exit;
            }

            $offset = intval($matches[1]);

            if (isset($matches[2])) {
                $end = intval($matches[2]);
                if ($offset > $end) {
                    header('HTTP/1.1 416 Requested Range Not Satisfiable');
                    header('Content-Range: bytes */' . $size);
                    exit;
                }
                $length = $end - $offset;
            } else {
                $length = $size - $offset;
            }
        }

        header('Content-Length: ' . $length);

        if ($partialContent) {
            header('HTTP/1.1 206 Partial Content');
            header('Content-Range: bytes ' . $offset . '-' . ($offset + $length - 1) . '/' . $size);
            // A full-length file will indeed be "bytes 0-x/x+1", think of 0-indexed array counts
        }

        $f = fopen($path, 'r');
        fseek($f, $offset);
        $pos = 0;

        while ($pos < $length) {
            $chunk = min($length - $pos, 1024 * 8);
            echo fread($f, $chunk);
            flush();
            ob_flush();
            $pos += $chunk;
        }
    }

    public function sortUp($id)
    {
        $current = Models\FContentVideo::find($id);
        $before = Models\FContentVideo::where('arrangement', $current->arrangement - 1)->first();

        $before->arrangement = $current->arrangement;
        $current->arrangement = $current->arrangement - 1;

        $current->save();
        $before->save();

        return \Redirect::back();
    }

    public function sortDown($id)
    {
        $current = Models\FContentVideo::find($id);
        $after = Models\FContentVideo::where('arrangement', $current->arrangement + 1)->first();

        $after->arrangement = $current->arrangement;
        $current->arrangement = $current->arrangement + 1;

        $current->save();
        $after->save();

        return \Redirect::back();
    }

    public function addMandatory($id)
    {
        Models\FContentVideo::find($id)->update(["mandatory" => 1]);

        return \Redirect::back();
    }

    public function removeMandatory($id)
    {
        Models\FContentVideo::find($id)->update(["mandatory" => 0]);

        return \Redirect::back();
    }
}
