<?php
namespace App\Http\Controllers;

use App\Libs\Vital;
use App\Models;
use App\Repositories\AcademicRepository as Academic;
use App\Repositories\LoginRepository as Login;

/**
 * PenggunaController Class
 *
 * Implements actions regarding user management
 *
 * Code for user type :
 *
 * 85a0c604388fe - Vital
 * 10a0c6043894a - Super Admin
 * 39a0c60438997 - Staff
 * 67a0c604389ec - Student
 * 34a0c60438a4b - Parent
 * 55a0c60438aaa - Reserve
 * 21a0c60438af6 - Reserve
 *
 * can be usefull for activation the user
 */
class LoginController extends Controller
{

    use Vital;

    public function __construct(Login $login, Academic $academic)
    {
        $this->user     = $login;
        $this->academic = $academic;
        \App::setLocale(\Session::get('locale'));
    }

    public function locale($locale)
    {
        \Session::set('locale', $locale);

        return \Redirect::back();
    }

    public function landingPage()
    {
        return \View::make('landing_page');
    }

    public function login()
    {
        if (\Auth::user()) {
            return \Redirect::to('dashboard');
        } else {
            return \View::make('login');
        }
    }

    public function doLogin()
    {
        $input = \Input::all();

        if ($this->user->login($input)) {
            if (\Auth::user()->role == 'Teacher' || \Auth::user()->role == 'Student') {
                if (\Session::get('session_default') != null) {
                    return \Redirect::intended('dashboard');
                } else {
                    return \Redirect::to('logout')
                                    ->withInput(\Input::except('password'))
                                    ->with('error', trans('login.no_session0'));
                }
            } else {
                return \Redirect::intended('dashboard');
            }
        } else {
            return \Redirect::to('login')
                            ->withInput(\Input::except('password'))
                            ->with('error', trans('login.invalid'));
        }
    }

    public function resendActivation($type, $id)
    {
        $data               = array();
        $data['from_email'] = $this->getDbConfig('from_email');
        $data['from_name']  = $this->getDbConfig('from_name');
        $data['vital_name'] = $this->getDbConfig('vital_name');
        if ($type == 'client') {
            $organization = Models\DClient::find($id);
            if ($organization->superAdmin->login->password != 'Not Active Yet!') {
                return \Redirect::back()->with('error', 'This account has been activated');
            }
            $data['email']           = $organization->superAdmin->email;
            $data['staff_name']      = $organization->superAdmin->staff_name;
            $data['activation_link'] = \URL::to('activate/' . $organization->superAdmin->id);
            \Mail::send('email.client_activation', $data, function ($message) use ($data) {
                $message->from($data['from_email'], $data['from_email']);
                $message->to($data['email'], $data['staff_name']);
                $message->subject('[' . $data['vital_name'] . '] Activate Your Account');
            });

            return \Redirect::back()->with('success', 'Resend successfully');
        } elseif ($type == 'staff') {
            $login = Models\CLogin::find($id);
            if ($login->password != 'Not Active Yet!') {
                return \Redirect::back()->with('error', 'This account has been activated');
            }
            $staff                   = Models\DStaff::find($id);
            $data['email']           = $staff->email;
            $data['staff_name']      = $staff->staff_name;
            $data['activation_link'] = \URL::to('activate/' . $staff->id);
            \Mail::send('email.staff_activation', $data, function ($message) use ($data) {
                $message->from($data['from_email'], $data['from_email']);
                $message->to($data['email'], $data['staff_name']);
                $message->subject('[' . $data['vital_name'] . '] Activate Your Account');
            });

            return \Redirect::back()->with('success', 'Resend successfully');
        } elseif ($type == 'student') {
            $login = Models\CLogin::find($id);
            if ($login->password != 'Not Active Yet!') {
                return \Redirect::back()->with('error', 'This account has been activated');
            }
            $student                 = Models\DStudent::find($id);
            $data['email']           = $student->email;
            $data['student_name']    = $student->student_name;
            $data['activation_link'] = \URL::to('activate/' . $student->id);
            \Mail::send('email.student_activation', $data, function ($message) use ($data) {
                $message->from($data['from_email'], $data['from_email']);
                $message->to($data['email'], $data['student_name']);
                $message->subject('[' . $data['vital_name'] . '] Activate Your Account');
            });

            return \Redirect::back()->with('success', 'Resend successfully');
        }
    }

    public function dashboard()
    {
        $dashboard = '';
        $data      = array(
            "title"      => trans('dashboard.menu'),
            'breadcrumb' => ''
        );

        if (\Auth::user()->role == 'Vital') {
            $dashboard = 'vital.dashboard';
        } elseif (\Auth::user()->role == 'Super Admin') {
            $dashboard = 'organization.dashboard';
        } elseif (\Auth::user()->role == 'Client (Coordinator)') {
            $dashboard = 'client_coordinator';
        } elseif (\Auth::user()->role == 'Teacher') {
            $dashboard   = 'teacher.dashboard';
            $data['log'] = \DB::table('f_lesson_history')
                              ->leftJoin('e_section', 'f_lesson_history.section_id', '=', 'e_section.id')
                              ->leftJoin('d_student', 'f_lesson_history.student_id', '=', 'd_student.id')
                              ->leftJoin('f_content_video', 'f_lesson_history.video_id', '=', 'f_content_video.id')
                              ->select(\DB::raw('d_student.student_name, f_content_video.title, f_lesson_history.updated_at'))
                              ->where('e_section.lecturer_id', 'like', '%' . \Auth::user()->user_id . '%')
                              ->orderBy('f_lesson_history.updated_at', 'DESC')
                              ->get();
        } elseif (\Auth::user()->role == 'Student') {
            $dashboard = 'student.dashboard';
        } elseif (\Auth::user()->role == 'Parent') {
            $dashboard = 'parent.dashboard';
        }

        return \View::make($dashboard, $data);
    }

    public function formActivateClient($id)
    {
        $login = $this->user->getLoginByUserId($id);
        if ($login == null) {
            exit ('User not found!');
        } elseif ($login->password != 'Not Active Yet!') {
            exit ('This account already activate!');
        }

        return \View::make('activate', array('id' => $id));
    }

    /*public function validateUsername()
    {
        $validator = \Validator::make(array('email' => \Input::get('email')), array('email' => 'required|email'), array('email' => 'Email yang dimasukkan tidak sah'));
        if ($validator->fails()) {
            return \Response::json($validator->errors()->first());
        }
        else {
            $pengguna = Models\APengguna::where('emel', \Input::get('email'))->get();
            if ($pengguna->count() > 0) {
                return \Response::json('Email pengguna telah wujud. Pengguna yang sama tidak boleh didaftarkan dua kali. Sila masukkan email yang betul');
            }
            else {
                return \Response::json($pengguna->count());
            }
        }
    }*/

    public function doActivateClient($id)
    {
        $input    = \Input::all();
        $rules    = [
            'username'     => 'required',
            'password'     => 'required|min:8',
            'rty_password' => 'required|same:password'
        ];
        $att      = [
            'rty_password' => 'retype password'
        ];
        $messages = array(
            'required' => trans('validation.required'),
            'same'     => trans('validation.same'),
            'min'      => trans('validation.min.string')
        );

        $validator = \Validator::make($input, $rules, $messages, $att);
        if ($validator->fails()) {
            return \Redirect::to('activate/' . $id)
                            ->withErrors($validator->messages())
                            ->withInput();
        } else {
            $login = $this->user->getLoginByUsername(array_get($input, 'username'));
            if ($login != null && $login->count() > 0) {
                return \Redirect::to('activate/' . $id)
                                ->with('error', 'Username has been choosen. Please select another username.')
                                ->withInput();
            } else {
                $this->user->storeLogin($id, $input);

                return \Redirect::to('login');
            }
        }
    }


    public function profile()
    {
        $data = array(
            "title"      => "Profail",
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>About Me</strong></li>',
        );

        return \View::make("vital.profile", $data);
    }

    /*public function changePassword()
    {
        if (\Request::isMethod('get')) {
            $data = array(
                "title" => "Tukar Katalaluan"
            );

            return \View::make("pengguna/change_password", $data);
        }
        else {
            $input     = \Input::all();
            $rules     = array(
                'katalaluan_asal'  => 'required',
                'katalaluan_baru'  => 'required|min:5',
                'katalaluan_ulang' => 'required|same:katalaluan_baru'
            );
            $messages  = array(
                'required'                  => 'Ruangan :attribute adalah mandatory',
                'katalaluan_ulang.required' => 'Ruangan sahkan katalaluan baru adalah mandatory',
                'same'                      => 'Katalaluan dan ulangannya mestilah sama',
                'min'                       => 'Katalaluan mesti sama atau lebih 5 huruf/angka'
            );
            $validator = \Validator::make($input, $rules, $messages);
            if ($validator->fails()) {
                return \Redirect::to('user/cpassword/' . \Request::segment(3))->withErrors($validator->messages());
            }
            else {
                $pengguna = Models\APengguna::where('katanama', \Auth::user()->katanama)->first();
                if ($pengguna != NULL && \Hash::check(array_get($input, 'katalaluan_asal'), \Auth::user()->katalaluan)) {
                    $pengguna->katalaluan = \Hash::make(array_get($input, 'katalaluan_baru'));
                    $pengguna->save();
                }
                else {
                    $bg = \Request::segment(2) == 'cpassword' ? 'Katalaluan Asal' : 'Katalaluan Pentadbir';

                    return \Redirect::to('user/cpassword/' . \Request::segment(3))->withErrors(array('message' => 'Sila pastikan ' . $bg . ' adalah betul'));
                }

                return \Redirect::to('user/cpassword/' . \Request::segment(3))->with('success', 'Tukar katalaluan berjaya');
            }
        }
    }*/

    /*
     * Pentadbir reset password pengguna tanpa email
     */
    /*public function resetPassword2()
    {
        if (\Request::isMethod('get')) {
            $data = array(
                "title" => "Tukar Katalaluan"
            );

            return \View::make("pengguna/change_password", $data);
        }
        else {
            $input     = \Input::all();
            $rules     = array(
                'katalaluan_asal'  => 'required',
                'katalaluan_baru'  => 'required|min:5',
                'katalaluan_ulang' => 'required|same:katalaluan_baru'
            );
            $messages  = array(
                'required'                  => 'Ruangan :attribute adalah mandatory',
                'katalaluan_ulang.required' => 'Ruangan sahkan katalaluan baru adalah mandatory',
                'same'                      => 'Katalaluan dan ulangannya mestilah sama',
                'min'                       => 'Katalaluan mesti sama atau lebih 5 huruf/angka'
            );
            $validator = \Validator::make($input, $rules, $messages);
            if ($validator->fails()) {
                return \Redirect::to('user/bpassword/' . \Request::segment(3))->withErrors($validator->messages());
            }
            else {
                $pengguna = Models\APengguna::find(\Request::segment(3));
                if ($pengguna != NULL && \Hash::check(array_get($input, 'katalaluan_asal'), \Auth::user()->katalaluan)) {
                    $pengguna->katalaluan = \Hash::make(array_get($input, 'katalaluan_baru'));
                    $pengguna->save();
                }
                else {
                    $bg = \Request::segment(2) == 'apassword' ? 'Katalaluan Asal' : 'Katalaluan Pentadbir';

                    return \Redirect::to('user/bpassword/' . \Request::segment(3))->withErrors(array('message' => 'Sila pastikan ' . $bg . ' adalah betul'));
                }

                return \Redirect::to('user')->with('success', 'Tukar katalaluan berjaya');
            }
        }
    }*/

    /*public function resetPassword($id)
    {
        $pengguna = Models\APengguna::where('id', $id)->first();
        if ($pengguna->emel != NULL || $pengguna->emel != '') {
            $pengguna->kod_pengesahan = md5(uniqid(mt_rand(), TRUE));
            $pengguna->save();
            \Config::set('services.mandrill.secret', Models\BKonfigurasi::find('mandrill_key')->nilai);
            \Config::set('mail.from', array('address' => Models\BKonfigurasi::find('daripada_mel')->nilai, 'name' => Models\BKonfigurasi::find('daripada_nama')->nilai));
            \Mail::send(
                'emel.reset_password',
                compact('pengguna'),
                function ($message) use ($pengguna) {
                    $message
                        ->to($pengguna->emel, $pengguna->nama)
                        ->subject('Reset Katalaluan');
                }
            );

            return \Redirect::route('user.index')->with('success', 'Reset katalaluan berjaya. Email panduan untuk reset katalaluan telah dihantar kepada pengguna.');
        }
        else {
            return \Redirect::route('user.index')->with('error', 'Tiada emel bagi pengguna. Sila kemaskini maklumat pengguna dengan memasukkan emel pengguna.');
        }
    }*/

    /*public function newPassword($token)
    {
        if (\Request::isMethod('get')) {
            $pengguna = Models\APengguna::where('kod_pengesahan', $token)->first();
            if ($pengguna) {
                return \View::make('new_password', array('action' => 'reset-password', 'token' => $token));
            }
            else {
                return \Redirect::to('login')->with('error', 'Token reset katalaluan tidak sah. Sila hubungi administrator');
            }
        }
        else {
            $input     = \Input::all();
            $rules     = array(
                'katalaluan'       => 'required|min:5',
                'ulang_katalaluan' => 'required|same:katalaluan'
            );
            $messages  = array(
                'required' => 'Ruangan :attribute adalah mandatory',
                'same'     => 'Katalaluan dan ulangannya mestilah sama',
                'min'      => 'Katalaluan mesti sama atau lebih 5 huruf/angka'
            );
            $validator = \Validator::make($input, $rules, $messages);
            if ($validator->fails()) {
                return \Redirect::to('reset-password/' . $token)->withErrors($validator->messages());
            }
            else {
                $pengguna                 = Models\APengguna::where('kod_pengesahan', $token)->first();
                $pengguna->kod_pengesahan = NULL;
                $pengguna->katalaluan     = \Hash::make(array_get($input, 'katalaluan'));
                $pengguna->aktif          = TRUE;
                $pengguna->save();

                return \Redirect::to('login')->with('success', 'Reset katalaluan berjaya. Sila login');
            }
        }
    }*/

    public function logout()
    {
        $default = '';
        if (\Session::get('error') == 'No session default') {
            $default = 'No session default';
        }
        \Auth::logout();
        \Session::clear();
        if ($default == 'No session default') {
            return \Redirect::to('login')->with('error', $default . '. Please contact system admin');
        } else {
            return \Redirect::to('login');
        }
    }
}
