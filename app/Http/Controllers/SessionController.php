<?php
namespace App\Http\Controllers;

class SessionController extends Controller
{

    public function __construct()
    {
        \App::setLocale(\Session::get('locale'));
    }

    public function index()
    {
        $data = array(
            'title'      => 'Option List',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">Configuration</a></li><li class="active"><strong>Option List</strong></li>',
        );

        return \View::make('organization.session', $data);
    }
}
