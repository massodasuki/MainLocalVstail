<?php
namespace App\Http\Controllers;

use App\Libs\Vital;
use App\Models\ESubject;
use App\Repositories\VitalRepository as VitalRepo;
use Ramsey\Uuid\Uuid;

class DeviceController extends Controller
{

    use Vital;

    protected $vital;

    public function __construct(VitalRepo $vital)
    {
        $this->vital = $vital;
        \App::setLocale(\Session::get('locale'));
    }

    public function device()
    {
        $data = array(
            'title'      => 'Devices',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>Vital Devices</strong></li>',
            'clients'    => array('' => '') + $this->vital->getClients()->lists('org_name', 'id')->toArray(),
            'devices'    => $this->vital->getDevices()->paginate(\Config::get('vital.option_paginate'))
        );

        return \View::make('vital.device', $data);
    }

    public function storeDevice()
    {
        $rules     = [
            'client_id' => 'required'
        ];
        $att       = [
            'client_id' => 'client'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages, $att);
        if (!$validator->fails()) {
            $device = $this->vital->storeDevice(\Input::all());
            if ($device['save_status']) {
                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'device'       => $device['device'],
                        'organization' => $device['device']->organization->org_name
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function deviceDetail($id)
    {
        $device      = $this->vital->getDevice($id);
        $mem         = explode('/', $device->memory);
        $swap        = explode('/', $device->swap);
        $space       = explode('/', $device->space);
        $getSubject  = json_decode($device->subject_id);
        $subjectHtml = '';

        if ($getSubject != null) {
            foreach ($getSubject as $gs) {
                $subject = ESubject::find($gs);
                $subjectHtml .= '<li>' . $subject->subject_name . '</li>';
            }
        }
        $resource = array(
            'total_memory' => $mem[1],
            'total_swap'   => $swap[1],
            'total_space'  => $space[1],
            'used_memory'  => number_format((($mem[1] - $mem[0]) / $mem[1]) * 100),
            'used_swap'    => number_format((($swap[1] - $swap[0]) / $swap[1]) * 100),
            'used_space'   => number_format((($space[1] - $space[0]) / $space[1]) * 100)
        );
        $data     = array(
            'title'       => 'Device Detail',
            'breadcrumb'  => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>Vital Devices</strong></li>',
            'device'      => $device,
            'subjectHtml' => $subjectHtml,
            'resource'    => $resource
        );

        return \View::make('vital.device_detail', $data);
    }

    public function editDevice($id)
    {
        $data = $this->vital->getDevice($id)->toArray();

        return \Response::json($data);
    }

    public function updateDevice($id)
    {
        $rules     = [
            'client_id' => 'required'
        ];
        $att       = [
            'client_id' => 'client'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages);
        if (!$validator->fails()) {
            $device = $this->vital->updateDevice(\Input::all(), $id);
            if ($device['save_status']) {
                return \Response::json(array(
                    'is_error' => false,
                    'msg'      => array(
                        'device'       => $device['device'],
                        'organization' => $device['device']->organization->org_name
                    )
                ));
            } else {
                return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
            }
        } else {
            return \Response::json(array('is_error' => true, 'error' => $validator->errors()));
        }
    }

    public function commandDevice()
    {
        $command = $this->vital->commandDevice(\Input::all());

        return \Response::json(array('is_error' => false));
    }

    public function deleteDevice($id)
    {
        $device = $this->vital->deleteDevice($id);

        return \Response::json(array(
            'is_error' => false,
            'msg'      => array(
                'device' => $device
            )
        ));
    }
}
