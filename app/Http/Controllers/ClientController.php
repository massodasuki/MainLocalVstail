<?php
namespace App\Http\Controllers;

use App\Libs\Vital;
use App\Models\ESection;
use App\Models\ESubject;
use App\Repositories\ClientRepository as Client;
use App\Repositories\SystemRepository as System;
use Ramsey\Uuid\Uuid;

class ClientController extends Controller
{

    use Vital;

    protected $client;

    protected $system;

    public function __construct(Client $client, System $system)
    {
        $this->client = $client;
        $this->system = $system;
        \App::setLocale(\Session::get('locale'));
    }

    public function staff()
    {
        $data = array(
            'title'      => 'All Staff',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>List All Staff</strong></li>',
        );

        return \View::make('organization.staff.index', $data);
    }

    public function createStaff()
    {
        $data = array(
            'title'      => trans('vital.create_staff'),
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('client') . '">' . trans('vital.list_staff') . '</a></li>
                             <li class="active"><strong>' . trans('vital.staff_form') . '</strong></li>',
            'position'   => $this->system->getOption('position', \Auth::user()->organization()->id)->lists('item', 'id')->all()
        );

        return \View::make('organization.staff.form', $data);
    }

    public function storeStaff()
    {
        $rules     = [
            'staff_name' => 'required',
            'role'       => 'required',
            'email'      => 'required|email'
        ];
        $att       = [
            'staff_name' => 'staff name'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages, $att);
        if (!$validator->fails()) {

            $file = \Input::file('cv');
            $cv   = array();
            if ($file != null) {
                $cv['asset_id']      = Uuid::uuid4()->getHex();
                $cv['original_name'] = $file->getClientOriginalName();
                $cv['new_name']      = $cv['asset_id'] . '.' . $file->getClientOriginalExtension();
                $file->move(env('ASSETS_STORAGE') . DIRECTORY_SEPARATOR . 'files', $cv['new_name']);
            }

            $file   = \Input::file('picture');
            $avatar = array();
            if ($file != null) {
                $avatar['asset_id']      = Uuid::uuid4()->getHex();
                $avatar['original_name'] = $file->getClientOriginalName();
                $avatar['new_name']      = $avatar['asset_id'] . '.' . $file->getClientOriginalExtension();
                $file->move(env('ASSETS_STORAGE') . DIRECTORY_SEPARATOR . 'images', $avatar['new_name']);
            }

            $client = $this->client->storeStaff($cv, $avatar, \Input::all());
            if ($client['status']) {
                $client['from_email']      = $this->getDbConfig('from_email');
                $client['from_name']       = $this->getDbConfig('from_name');
                $client['vital_name']      = $this->getDbConfig('vital_name');
                $client['activation_link'] = \URL::to('activate/' . $client['staff_id']);
                \Mail::send('email.staff_activation', $client, function ($message) use ($client) {
                    $message->from($client['from_email'], $client['from_email']);
                    $message->to($client['email'], $client['staff_name']);
                    $message->subject('[' . $client['vital_name'] . '] Activate Your Account');
                });
            }
        } else {
            return \Redirect::to('create/staff')
                            ->withInput()
                            ->withErrors($validator);
        }

        return \Redirect::to('staff');
    }

    public function editStaff($id)
    {
        $data = array(
            'title'      => trans('vital.edit_staff'),
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('client') . '">' . trans('vital.edit_staff') . '</a></li>
                             <li class="active"><strong>' . trans('vital.edit_form') . '</strong></li>',
            'position'   => $this->system->getOption('Position', \Auth::user()->organization()->id)->lists('item', 'id')->all(),
            'staff'      => $this->client->getStaff($id)
        );

        return \View::make('organization.staff.form', $data);
    }

    public function updateStaff($id)
    {
        $rules    = [
            'staff_name' => 'required',
            'role'       => 'required',
            'email'      => 'required|email'
        ];
        $att      = [
            'staff_name' => 'staff name'
        ];
        $messages = ['required' => trans('validation.required')];

        $validator = \Validator::make(\Input::all(), $rules, $messages, $att);
        if (!$validator->fails()) {
            $staff = $this->client->getStaff($id);
            $file  = \Input::file('cv');
            $cv    = array();
            if ($file != null) {
                $cv['asset_id']      = Uuid::uuid4()->getHex();
                $cv['original_name'] = $file->getClientOriginalName();
                $cv['new_name']      = $cv['asset_id'] . '.' . $file->getClientOriginalExtension();
                $file->move(env('ASSETS_STORAGE') . DIRECTORY_SEPARATOR . 'files', $cv['new_name']);
                if ($staff->cv != null && \Storage::disk('assets')->exists('files' . DIRECTORY_SEPARATOR . $this->getFile($staff->cv))) {
                    \Storage::disk('assets')->delete('files' . DIRECTORY_SEPARATOR . $this->getFile($staff->cv));
                }
            }

            $file   = \Input::file('picture');
            $avatar = array();
            if ($file != null) {
                $avatar['asset_id']      = Uuid::uuid4()->getHex();
                $avatar['original_name'] = $file->getClientOriginalName();
                $avatar['new_name']      = $avatar['asset_id'] . '.' . $file->getClientOriginalExtension();
                $file->move(env('ASSETS_STORAGE') . DIRECTORY_SEPARATOR . 'images', $avatar['new_name']);
                if ($staff->avatar != null && \Storage::disk('assets')->exists('images' . DIRECTORY_SEPARATOR . $this->getFile($staff->avatar))) {
                    \Storage::disk('assets')->delete('images' . DIRECTORY_SEPARATOR . $this->getFile($staff->avatar));
                }
            }

            $this->client->updateStaff($staff, $cv, $avatar, \Input::all());
        } else {
            return \Redirect::to('edit/staff/' . $id)
                            ->withInput()
                            ->withErrors($validator);
        }

        return \Redirect::to('staff');
    }

    public function showStaff($id)
    {
        $staff = $this->client->getStaff($id);
        $data  = array(
            'title'      => trans('vital.show_staff'),
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>' . trans('vital.show_staff') . '</strong></li>',
            'staff'      => $staff,
            'avatar'     => $staff->avatar == null ? \URL::to('assets/images/no_picture.png') : \URL::to('assets/images/' . $this->getFile($staff->avatar))
        );

        return \View::make('organization.staff.show', $data);
    }

    public function deleteStaff($id)
    {
        $this->client->deleteStaff($id);

        return \Redirect::to('staff');
    }

    public function student()
    {
        $data = array(
            'title'      => trans('vital.list_student'),
            'menu'       => 'student',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>Student</strong></li>',
        );

        if (\Auth::user()->role == 'Super Admin') {
            return \View::make('organization.student.index', $data);
        } elseif (\Auth::user()->role == 'Teacher') {
            $section    = ESection::where('session_id', \Session::get('session_default'))->where('lecturer_id', 'like',
                '%' . \Auth::user()->user_id . '%')->get();
            $student_id = array();
            foreach ($section as $sec) {
                $student_id = array_merge($student_id, json_decode($sec->student_id));
            }
            $data['students'] = $this->client->getStudentsIn($student_id)->paginate(\Config::get('vital.option_paginate'));

            return \View::make('teacher.student', $data);
        } else {
            return \Redirect::to('/');
        }
    }


    public function createStudent()
    {
        $data = array(
            'title'      => trans('vital.create_student'),
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('client') . '">' . trans('vital.create_student') . '</a></li>
                             <li class="active"><strong>' . trans('vital.student_form') . '</strong></li>',
        );

        return \View::make('organization.student.form', $data);
    }

    public function storeStudent()
    {
        $rules     = [
            'a_student_name' => 'required',
            'a_email'        => 'required|email'
        ];
        $att       = [
            'a_student_name' => 'student name'
        ];
        $messages  = ['required' => trans('validation.required')];
        $validator = \Validator::make(\Input::all(), $rules, $messages, $att);
        if (!$validator->fails()) {
            $file   = \Input::file('a_picture_id');
            $avatar = array();
            if ($file != null) {
                $avatar['asset_id']      = Uuid::uuid4()->getHex();
                $avatar['original_name'] = $file->getClientOriginalName();
                $avatar['new_name']      = $avatar['asset_id'] . '.' . $file->getClientOriginalExtension();
                $file->move(env('ASSETS_STORAGE') . DIRECTORY_SEPARATOR . 'images', $avatar['new_name']);
            }

            $client = $this->client->storeStudent($avatar, \Input::all());
            if ($client['status']) {
                $client['from_email']      = $this->getDbConfig('from_email');
                $client['from_name']       = $this->getDbConfig('from_name');
                $client['vital_name']      = $this->getDbConfig('vital_name');
                $client['activation_link'] = \URL::to('activate/' . $client['student_id']);
                \Mail::send('email.student_activation', $client, function ($message) use ($client) {
                    $message->from($client['from_email'], $client['from_email']);
                    $message->to($client['email'], $client['student_name']);
                    $message->subject('[' . $client['vital_name'] . '] Activate Your Account');
                });
            }
        } else {
            return \Redirect::to('create/student')
                            ->withInput()
                            ->withErrors($validator);
        }

        return \Redirect::to('student');
    }

    public function editStudent($id)
    {
        $student = $this->client->getStudent($id);
        $parent  = $student->parent == null ? null : $this->client->getParent($student->parent->id);
        $data    = array(
            'title'      => trans('vital.edit_student'),
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('client') . '">' . trans('vital.edit_student') . '</a></li>
                             <li class="active"><strong>' . trans('vital.edit_form') . '</strong></li>',
            'student'    => $student,
            'parent'     => $parent
        );

        return \View::make('organization.student.form', $data);
    }

    public function updateStudent($id)
    {
        $rules    = [
            'a_student_name' => 'required',
            'a_email'        => 'required|email'
        ];
        $att      = [
            'a_student_name' => 'student name'
        ];
        $messages = ['required' => trans('validation.required')];

        $validator = \Validator::make(\Input::all(), $rules, $messages, $att);
        if (!$validator->fails()) {
            $student = $this->client->getStudent($id);
            $file    = \Input::file('a_picture_id');
            $avatar  = array();
            if ($file != null) {
                $avatar['asset_id']      = Uuid::uuid4()->getHex();
                $avatar['original_name'] = $file->getClientOriginalName();
                $avatar['new_name']      = $avatar['asset_id'] . '.' . $file->getClientOriginalExtension();
                $file->move(env('ASSETS_STORAGE') . DIRECTORY_SEPARATOR . 'images', $avatar['new_name']);
                if ($student->avatar != null && \Storage::disk('assets')->exists('images' . DIRECTORY_SEPARATOR . $this->getFile($student->avatar))) {
                    \Storage::disk('assets')->delete('images' . DIRECTORY_SEPARATOR . $this->getFile($student->avatar));
                }
            }

            $this->client->updateStudent($avatar, $student, \Input::all());
            //TODO buat satu fungsi untuk resend activation. Edit email, kemudian kena resend activation email secara manual menggunakan butang
        } else {
            return \Redirect::to('edit/student/' . $id)
                            ->withInput()
                            ->withErrors($validator);
        }

        return \Redirect::to('student');
    }

    public function showStudent($id)
    {
        $student = $this->client->getStudent($id);
        $parent  = $student->parent == null ? null : $this->client->getParent($student->parent->id);
        $data    = array(
            'title'      => trans('vital.show_student'),
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li><a href="' . \URL::to('client') . '">' . trans('vital.show_student') . '</a></li>
                             <li class="active"><strong>' . trans('vital.show_form') . '</strong></li>',
            'student'    => $student,
            'parent'     => $parent,
            'avatar'     => $student->avatar == null ? \URL::to('assets/images/no_picture.png') : \URL::to('assets/images/' . $this->getFile($student->avatar))
        );

        return \View::make('organization.student.show', $data);
    }

    public function device()
    {
        $data = array(
            'title'      => 'Devices',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>Vital Devices</strong></li>',
            'devices'    => $this->client->getDevices()->get()
        );

        return \View::make('organization.device', $data);
    }

    public function deviceDetail($id)
    {
        $device   = $this->client->getDevice($id);
        $mem      = explode('/', $device->memory);
        $swap     = explode('/', $device->swap);
        $space    = explode('/', $device->space);
        $getSubject = json_decode($device->subject_id);
        $subjectHtml = '';

        if ($getSubject != null) {
            foreach ($getSubject as $gs) {
                $subject = ESubject::find($gs);
                $subjectHtml .= '<li>' . $subject->subject_name . '</li>';
            }
        }
        
        $resource = array(
            'total_memory' => $mem[1],
            'total_swap'   => $swap[1],
            'total_space'  => $space[1],
            'used_memory'  => number_format((($mem[1] - $mem[0]) / $mem[1]) * 100),
            'used_swap'    => number_format((($swap[1] - $swap[0]) / $swap[1]) * 100),
            'used_space'   => number_format((($space[1] - $space[0]) / $space[1]) * 100)
        );
        $data     = array(
            'title'      => 'Device Detail',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">' . trans('vital.dashboard') . '</a></li>
                             <li class="active"><strong>Vital Devices</strong></li>',
            'device'     => $device,
            'resource'   => $resource,
            'subjectHtml'   => $subjectHtml,
            'subjects'   => ESubject::where('organization_id', \Auth::user()->organization()->id)->lists('subject_name', 'id')
        );

        return \View::make('organization.device_detail', $data);
    }

    public function updateDevice($id)
    {
        $device = $this->client->updateDevice(\Input::all(), $id);
        if ($device['save_status']) {
            $subjects = ESubject::select('subject_name')->whereIn('id', array_get(\Input::all(), 'subjects'))->get()->toArray();
            return \Response::json(array(
                'is_error' => false,
                'msg'      => array(
                    'subjects'       => $subjects,
                    'location' => array_get(\Input::all(), 'location')
                )
            ));
        } else {
            return \Response::json(array('is_error' => true, 'error' => 'Error saving data to database!'));
        }
    }
}
