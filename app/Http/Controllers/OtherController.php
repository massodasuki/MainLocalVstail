<?php
namespace App\Http\Controllers;

use App\Libs\Vital;
use App\Repositories\SystemRepository as System;

class OtherController extends Controller
{

    use Vital;

    protected $system;

    public function __construct(System $system)
    {
        $this->system = $system;
        \App::setLocale(\Session::get('locale'));
    }

    public function indexConfig()
    {
        $data = array(
            'title'      => 'Option List',
            'breadcrumb' => '<li><a href="' . \URL::to('/') . '">Configuration</a></li><li class="active"><strong>Option List</strong></li>',
        );
        if (\Auth::user()->role == 'Vital') {
            $vital_config = $this->system->getConfig(\Auth::user()->user_id);
            $config       = array();
            foreach ($vital_config as $vc) {
                $config[$vc->param] = $vc->value;
            }
            $data['config'] = $config;

            return \View::make('vital.config', $data);
        } elseif (\Auth::user()->role == 'Super Admin') {
            $vital_config = $this->system->getConfig(\Auth::user()->organization()->id);
            $config       = array();
            foreach ($vital_config as $vc) {
                $config[$vc->param] = $vc->value;
            }
            $data['config'] = $config;

            return \View::make('organization.config', $data);
        }
    }

    public function updateConfig()
    {
        if (\Auth::user()->role == 'Vital') {
            $this->system->updateConfig(\Input::all(), \Auth::user()->user_id);
        } elseif (\Auth::user()->role == 'Super Admin') {
            $this->system->updateConfig(\Input::all(), \Auth::user()->organization()->id);
        }

        return \Response::json('Success');
    }

    public function storeOption()
    {
        if (\Auth::user()->role == 'Vital') {
            $this->system->storeOption(\Input::all(), \Auth::user()->user_id);
        } elseif (\Auth::user()->role == 'Super Admin') {
            $this->system->storeOption(\Input::all(), \Auth::user()->organization()->id);
        }

        return \Response::json('Success');
    }

    public function editOption($id)
    {
        $data                          = $this->system->editOption($id)->toArray();
        $data['id']                    = $data['id'];
        $data['vital_or_organization'] = $data['vital_or_organization'];

        return \Response::json($data);
    }

    public function updateOption($id)
    {
        $this->system->updateOption($id, \Input::all());

        return \Response::json("success");
    }

    //TODO: check relation before delete option
    public function deleteOption($id)
    {
        $this->system->deleteOption($id);

        return \Response::json("success");
    }

    public function device()
    {
        if (\Auth::user()->role == 'Vital') {
            return \App::make('App\Http\Controllers\VitalController')->device();
        } elseif (\Auth::user()->role == 'Super Admin') {
            return \App::make('App\Http\Controllers\ClientController')->device();
        } else {
            return \Redirect::to('/');
        }
    }

    public function deviceDetail($id)
    {
        if (\Auth::user()->role == 'Vital') {
            return \App::make('App\Http\Controllers\VitalController')->deviceDetail($id);
        } elseif (\Auth::user()->role == 'Super Admin') {
            return \App::make('App\Http\Controllers\ClientController')->deviceDetail($id);
        } else {
            return \Redirect::to('/');
        }
    }

    public function deviceUpdate($id)
    {
        if (\Auth::user()->role == 'Vital') {
            return \App::make('App\Http\Controllers\VitalController')->updateDevice($id);
        } elseif (\Auth::user()->role == 'Super Admin') {
            return \App::make('App\Http\Controllers\ClientController')->updateDevice($id);
        } else {
            return \Response::json(array('is_error' => true, 'error' => 'You are not authenticated to modify this!'));
        }
    }

    public function deviceCommand()
    {
            return \App::make('App\Http\Controllers\VitalController')->commandDevice();
    }
}
