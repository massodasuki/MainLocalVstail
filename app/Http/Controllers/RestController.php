<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models;
use Illuminate\Http\Request;
use JWTAuth;
use Ramsey\Uuid\Uuid;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class RestController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    public function authenticate(Request $request)
    {
        $user = Models\CLogin::where('rest_token', $request->get('token'))->first();

        if ($user) {
            $token = JWTAuth::fromUser($user);

            // if no errors are encountered we can return a JWT
            return response()->json(compact('token'));
        } else {
            return response()->json('Invalid token!');
        }
    }

    public function ping(Request $request)
    {
        $auth = $this->jwtUser();

        if ($auth['error']) {
            return response()->json(['error' => $auth['details']], $auth['code']);
        }

        if ($auth) {
            $ping            = new Models\GPing();
            $ping->id        = Uuid::uuid4()->getHex();
            $ping->device_id = $auth->user_id;
            $ping->ping_at   = date('Y-m-d H:i:') . rand(10, 25);
            $ping->save();
            \DB::table('g_device')->update(array(
                'ip_address' => $request->get('ip'),
                'memory'     => $request->get('memory'),
                'swap'       => $request->get('swap'),
                'space'      => $request->get('space')
            ));
        } else {
            return response()->json('Invalid key!');
        }
    }

    public function command(Request $request)
    {
        $auth = $this->jwtUser();

        if ($auth['error']) {
            return response()->json(['error' => $auth['details']], $auth['code']);
        }

        if ($auth) {
            $commands = Models\GDeviceLog::where('device_id', $request->get('device_id'))->whereNull('finish_at')->get();
            if ($commands->count() > 0) {
                $return = $commands->toArray();
                foreach ($commands as $command) {
                    $command->finish_at = date('Y-m-d H:i:s');
                    $command->save();
                }

                return response()->json($return);
            } else {
                return response()->json(null);
            }
        } else {
            return response()->json('Invalid key!');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activate(Request $request)
    {
        $jwtUser = $this->jwtUser();
        if ($jwtUser['error']) {
            return response()->json(['error' => $jwtUser['details']], $jwtUser['code']);
        }

        $device = Models\GDevice::find($jwtUser->user_id);

        if ($request->get('serial') != $device->serial_number) {
            return response()->json(['error' => 'Invalid serial', 'admin' => $device->organization->superAdmin]);
        }

        if (!$device->subject_id) {
            return response()->json(['error' => 'Subject missing', 'admin' => $device->organization->superAdmin]);
        } else {
            $organization  = $device->organization;
            $subjects      = json_decode($device->subject_id);
            $data          = array();
            $assets        = array();
            $logins        = array();

            $a_assets      = Models\AAssets::where('for', 'Organization Logo')->where('for_id', $organization->id)->get();
            if ($a_assets->count() > 0) {
                array_push($assets, $a_assets->toArray());
            }

            $a_config         = Models\AConfig::where('vital_or_organization', $organization->id)->get();
            $data['a_config'] = $a_config->toArray();

            $a_option         = Models\AOption::where('vital_or_organization', $organization->id)->get();
            $data['a_option'] = $a_option->toArray();

            $d_organization         = Models\DClient::find($organization->id);
            $data['d_organization'] = [$d_organization->toArray()];

            $d_staff         = Models\DStaff::where('organization_id', $organization->id)->get();
            $data['d_staff'] = $d_staff->toArray();

            $d_student         = Models\DStudent::where('organization_id', $organization->id)->get();
            $data['d_student'] = $d_student->toArray();
            foreach ($d_student as $student) {
                $a_assets = Models\AAssets::where('for', 'Student Avatar')->where('for_id', $student->id)->get();
                if ($a_assets->count() > 0) {
                    array_push($assets, $a_assets->toArray());
                }
                if ($student->parent) {
                    $data['d_parent'] = [$student->parent->toArray()];
                }
                if ($student->watchHistory) {
                    $data['f_lesson_history'] = [$student->watchHistory->toArray()];
                }
                if ($student->login) {
                    array_push($logins, $student->login->toArray());
                    /*if ($staff->login->log) {
                        $data['c_login_log'] = [$staff->login->log->toArray()];
                    }*/
                }
            }

            foreach ($d_staff as $staff) {
                $a_assets = Models\AAssets::where('for', 'Staff Avatar')->where('for_id', $staff->id)->get();
                if ($a_assets->count() > 0) {
                    array_push($assets, $a_assets->toArray());
                }
                $a_assets = Models\AAssets::where('for', 'Staff CV')->where('for_id', $staff->id)->get();
                if ($a_assets->count() > 0) {
                    array_push($assets, $a_assets->toArray());
                }
                if ($staff->login) {
                    array_push($logins, $staff->login->toArray());
                    /*if ($staff->login->log) {
                        $data['c_login_log'] = [$staff->login->log->toArray()];
                    }*/
                }
            }

            $e_course         = Models\ECourse::where('organization_id', $organization->id)->get();
            $data['e_course'] = $e_course->toArray();

            $e_section         = Models\ESection::where('organization_id', $organization->id)->get();
            $data['e_section'] = $e_section->toArray();

            $e_session         = Models\ESession::where('organization_id', $organization->id)->get();
            $data['e_session'] = $e_session->toArray();

            $e_subject         = Models\ESubject::whereIn('id', $subjects)->get();
            $data['e_subject'] = $e_subject->toArray();

            //$f_announcement    = Models\FAnnouncement::whereRaw($condition)->get();

            $f_content_other         = Models\FContentOther::whereIn('subject_id', $subjects)->get();
            $data['f_content_other'] = $f_content_other->toArray();
            foreach ($f_content_other as $other) {
                $a_assets = Models\AAssets::where('for', 'Content Other')->where('for_id', $other->id)->get();
                if ($a_assets->count() > 0) {
                    array_push($assets, $a_assets->toArray());
                }
            }

            $f_content_video         = Models\FContentVideo::whereIn('subject_id', $subjects)->get();
            $data['f_content_video'] = $f_content_video->toArray();
            foreach ($f_content_video as $video) {
                $a_assets = Models\AAssets::where('for', 'Content Video Original')->where('for_id', $video->id)->get();
                if ($a_assets->count() > 0) {
                    array_push($assets, $a_assets->toArray());
                }

                $a_assets = Models\AAssets::where('for', 'Content Thumbnail')->where('for_id', $video->id)->get();
                if ($a_assets->count() > 0) {
                    array_push($assets, $a_assets->toArray());
                }
            }
            $data['a_assets'] = $assets;
            $data['c_login']  = $logins;
            $data['device_id']  = $device->id;

            return response()->json($data);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function timeDifferent()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['error' => 'User not found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['error' => 'Token expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['error' => 'Token invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['error' => 'Token absent'], $e->getStatusCode());
        }

        $device = Models\GDevice::where('serial_number', $user->rest_token)->first();
        if (!$device->subject_id) {
            return response()->json(['error' => 'Subject missing', 'admin' => $device->organization->superAdmin]);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function synchronize(Request $request)
    {
        $table     = $request->get('table');
        $last_sync = $request->get('last_sync');
        $record    = json_decode($request->get('record'), JSON_NUMERIC_CHECK);
        $response  = array();
        dd($record);
        switch ($table) {
            case "a_assets":
                $isExists = Models\AAssets::find($record['id']);
                $this->updateDb("a_assets", $isExists, $record, new Models\AAssets());
                break;
            case "a_config":
                $isExists = Models\AConfig::find($record['id']);
                $this->updateDb("a_config", $isExists, $record, new Models\AConfig());
                break;
            case "a_option":
                $isExists = Models\AOption::find($record['id']);
                $this->updateDb("a_option", $isExists, $record, new Models\AOption());
                break;
            case "c_login":
                $isExists = Models\CLogin::find($record['user_id']);
                $this->updateDb("a_option", $isExists, $record, new Models\CLogin());
                break;
            case "c_login_log":
                $isExists = Models\CLoginLog::find($record['id']);
                $this->updateDb("c_login_log", $isExists, $record, new Models\CLoginLog());
                break;
            case "d_parent":
                $isExists = Models\DParent::find($record['id']);
                $this->updateDb("d_parent", $isExists, $record, new Models\DParent());
                break;
            case "d_staff":
                $isExists = Models\DStaff::find($record['id']);
                $this->updateDb("d_parent", $isExists, $record, new Models\DStaff());
                break;
            case "d_student":
                $isExists = Models\DStudent::find($record['id']);
                $this->updateDb("d_student", $isExists, $record, new Models\DStudent());
                break;
            case "e_course":
                $isExists = Models\ECourse::find($record['id']);
                $this->updateDb("e_course", $isExists, $record, new Models\ECourse());
                break;
            case "e_section":
                $isExists = Models\ESection::find($record['id']);
                $this->updateDb("e_section", $isExists, $record, new Models\ESection());
                break;
            case "e_session":
                $isExists = Models\ESession::find($record['id']);
                $this->updateDb("e_session", $isExists, $record, new Models\ESession());
                break;
            case "e_subject":
                $isExists = Models\ESubject::find($record['id']);
                $this->updateDb("e_subject", $isExists, $record, new Models\ESubject());
                break;
            case "f_announcement":
                $isExists = Models\FAnnouncement::find($record['id']);
                $this->updateDb("f_announcement", $isExists, $record, new Models\FAnnouncement());
                break;
            case "f_content_other":
                $isExists = Models\FContentOther::find($record['id']);
                $this->updateDb("f_content_other", $isExists, $record, new Models\FContentOther());
                break;
            case "f_content_video":
                $isExists = Models\FContentVideo::find($record['id']);
                $this->updateDb("f_content_video", $isExists, $record, new Models\FContentVideo());
                break;
            case "f_lesson_history":
                $isExists = Models\FLessonHistory::find($record['id']);
                $this->updateDb("f_lesson_history", $isExists, $record, new Models\FLessonHistory());
                break;
        }

        return response()->json($response);
    }

    protected function updateDb($table, $isExists, $record, $new)
    {
        if ($isExists == null) {
            foreach ($record as $key => $value) {
                $new->$key = $value;
            }
            $new->save();
        } else {
            if (date_create($isExists['updated_at']) < date_create($record['updated_at'])) {
                foreach ($record as $key => $value) {
                    if ($key != 'id') {
                        $isExists->$key = $value;
                    }
                }
                $isExists->save();
            }
        }
        $response['berjaya'] = 'yeah';
    }

    protected function jwtUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return array('error' => true, 'details' => 'User not found', 'code' => 404);
            }
        } catch (TokenExpiredException $e) {
            return array('error' => true, 'details' => 'Token expired', 'code' => $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return array('error' => true, 'details' => 'Token invalid', 'code' => $e->getStatusCode());
        } catch (JWTException $e) {
            return array('error' => true, 'details' => 'Token absent', 'code' => $e->getStatusCode());
        }

        return $user;
    }
}
