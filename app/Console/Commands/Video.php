<?php

namespace App\Console\Commands;

use App\Libs\Vital;
use App\Models\FContentVideo;
use App\Models\FProcessQueue;
use GuzzleHttp;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class Video extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'video';

    protected $videoPath;

    use Vital;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encode the video';

    public function __construct()
    {
        parent::__construct();
        $this->videoPath = public_path('assets' . DIRECTORY_SEPARATOR . 'contents' . DIRECTORY_SEPARATOR . 'videos' . DIRECTORY_SEPARATOR);
    }

    public function handle()
    {

        //Get action to video, either encode, create watermark, or track progress
        $action = $this->argument('action');

        if ($action == 'encode') {
            if (FProcessQueue::where('process', 'encode')->where('status', 'ongoing')->get()->count() == 0) {
                $queue = FProcessQueue::whereIn('process', array('encode webm', 'encode mp4'))->whereNull('status')->orderBy('created_at', 'ASC')->first();
                if ($queue) {
                    $getID3        = new \getID3();
                    $file          = $this->getFile($queue->asset);
                    $id3           = $getID3->analyze($this->videoPath . $file);
                    $queue->status = 2;
                    $queue->log = null;
                    $queue->start_datetime = date('Y-m-d H:i:s');
                    $queue->save();
                    if ($queue->process == 'encode webm') {
                        shell_exec('ffmpeg -y -threads 1 -i "' . $this->videoPath . $file . '" ' .
                                   '-filter:v fps=' . $id3['video']['frame_rate'] . ' -c:v ' .
                                   'libvpx -c:a libvorbis -pix_fmt yuv420p -b:v 2M -crf 5 "' . $this->videoPath . $queue->asset->id . '.webm" ' .
                                   '2>&1 | tee "' . $this->videoPath . 'log.txt"');
                        $video = FContentVideo::find($queue->asset->for_id);
                        //$video->webm = 
                    } elseif ($queue->process == 'encode mp4') {
                        shell_exec(' ffmpeg -y -threads 1 -i "' . $this->videoPath . $file . '" ' .
                                   '-filter:v fps=' . $id3['video']['frame_rate'] . ' -c:v ' .
                                   'libx264 -pix_fmt yuv420p -profile:v baseline -preset slower -crf 23 -vf "scale=trunc(in_w/2)*2:trunc(in_h/2)*2" ' .
                                   '-movflags +faststart "' . $this->videoPath . $queue->asset->id . '.mp4" 2>&1 | tee "' . $this->videoPath . 'log.txt');
                    }
                    
                    $queue->status = 1;
                    $queue->progress = 100;
                    $queue->end_datetime = date('Y-m-d H:i:s');
                    $queue->log = @file_get_contents($this->videoPath . 'log.txt');
                    $queue->save();
                }
            }
        } elseif ($action == 'watermark') {

        } elseif ($action == 'progress') {
            $queue = FProcessQueue::whereIn('process', array('encode webm', 'encode mp4'))->where('status', 2)->first();
            if ($queue) {
                $content = @file_get_contents($this->videoPath . 'log.txt');

                if ($content) {
                    //get duration of source
                    preg_match("/Duration: (.*?), start:/", $content, $matches);

                    $rawDuration = $matches[1];

                    //rawDuration is in 00:00:00.00 format. This converts it to seconds.
                    $ar       = array_reverse(explode(":", $rawDuration));
                    $duration = floatval($ar[0]);
                    if (!empty($ar[1])) {
                        $duration += intval($ar[1]) * 60;
                    }
                    if (!empty($ar[2])) {
                        $duration += intval($ar[2]) * 60 * 60;
                    }

                    //get the time in the file that is already encoded
                    preg_match_all("/time=(.*?) bitrate/", $content, $matches);

                    $rawTime = array_pop($matches);

                    //this is needed if there is more than one match
                    if (is_array($rawTime)) {
                        $rawTime = array_pop($rawTime);
                    }

                    //rawTime is in 00:00:00.00 format. This converts it to seconds.
                    $ar   = array_reverse(explode(":", $rawTime));
                    $time = floatval($ar[0]);
                    if (!empty($ar[1])) {
                        $time += intval($ar[1]) * 60;
                    }
                    if (!empty($ar[2])) {
                        $time += intval($ar[2]) * 60 * 60;
                    }

                    //calculate the progress
                    $progress = round(($time / $duration) * 100);

                    //$this->info($progress . "%");

                    $queue->progress = $progress;
                    $queue->log      = $content;
                    $queue->save();
                }
            }
        } elseif ($action == 's3') {

        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('action', InputArgument::REQUIRED, 'What to do with the video'),
        );
    }
}
