<?php

namespace App\Console\Commands;

use GuzzleHttp;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class Inspire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'inspire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display an inspiring quote';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    /*public function handle()
    {
        /*$this->output->progressStart(2000000);
        //$name = $this->ask('What is your name?');
        //$this->comment(PHP_EOL . $name . PHP_EOL);
        for($i=0; $i < 2000000; $i++) {
            \DB::insert('INSERT INTO a_content_temp (user_id, filename) VALUES(?,?)', array('a', $i));
            $this->output->progressAdvance();
        }
        $this->output->progressFinish();
        $this->error("This is an error.");
    }*/

    public function handle()
    {
        /*$this->info("Creating progress bar...\n");

        $progress = $this->output->createProgressBar(100);

        $progress->getProgress();
        $progress->setFormat("%message%\n %current%/%max% [%bar%] %percent:3s%%");

        $progress->setMessage("100? I won't count all that!");
        $progress->setProgress(60);

        for ($i = 0; $i < 40; $i++) {
            sleep(1);
            if ($i == 90) {
                $progress->setMessage('almost there...');
            }
            $progress->advance();
        }

        $progress->finish();

        $this->line('Welcome to the user generator.');*/

        $client = new GuzzleHttp\Client();
        $res    = $client->request('POST', 'http://vital.app/api/v1/authenticate/', [
            'query' => ['username' => 'fattah', 'password' => '0199218793']
        ]);
        /*$res = $client->request('GET', 'http://vital.app/api/v1/users', [
                    'query' => ['token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL3ZpdGFsLmFwcFwvYXBpXC92MVwvYXV0aGVudGljYXRlIiwiaWF0IjoxNDU0Mzk4ODc5LCJleHAiOjE0NTQ0MDI0NzksIm5iZiI6MTQ1NDM5ODg3OSwianRpIjoiOTUzZTdkNGNjNDIwNTMzMjAwNzE0YzdmNjEyZDVhZDYifQ.9kCW6j3y3wpA33Pqlvpfd-yrJpdQFzzqSQGXZsMG63I']
                ]);*/

        var_dump(json_decode($res->getBody()));

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('name', InputArgument::REQUIRED, 'Name of the new user'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('age', null, InputOption::VALUE_REQUIRED, 'Age of the new user')
        );
    }
}
