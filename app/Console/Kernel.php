<?php

namespace App\Console;

use App\Console\Commands\Video;
use App\Models\GDevice;
use App\Models\GPing;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Ramsey\Uuid\Uuid;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Video::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('video progress')->everyMinute();

        // If device down, log zero/0 second on that device for this time/minute. so, when second 0 appear, we knows the device is down on the minutes.
        $schedule->call(function () {
            $datetime = Carbon::now()->subMinute(2)->format('Y-m-d H:i');
                $devices = GDevice::all();
                foreach ($devices as $device) {
                    $test = GPing::whereRaw('DATE_FORMAT(ping_at, "%Y-%m-%d %H:%i") = ? AND device_id = ?', [$datetime, $device->id])->first();
                    if ($test == null) {
                        $ping          = new GPing();
                        $ping->id      = Uuid::uuid4()->getHex();
                        $ping->device_id      = $device->id;
                        $ping->ping_at = $datetime . ':00';
                        $ping->save();
                    }
                }
        })->everyMinute();
    }
}
