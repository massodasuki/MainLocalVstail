<?php
namespace App\Repositories;

use App\Libs\Vital;
use App\Models\DStaff;
use App\Models\DStudent;
use App\Models\ECourse;
use App\Models\ESection;
use App\Models\ESession;
use App\Models\ESubject;
use App\Models\FQuizQuestion;
use App\Models\GQuestion;
use Ramsey\Uuid\Uuid;

/**
 * Class ContentRepository
 * @package App\Repositories
 *
 * Repository ini akan berfungsi untuk interaksi dengan database bagi content pengajaran
 *
 */
class AcademicRepository
{
    use Vital;

    public function getLecturers()
    {
        return DStaff::leftJoin('c_login', 'd_staff.id', '=', 'c_login.user_id')->where('d_staff.organization_id', \Auth::user()
                                                                                                                        ->organization()->id)->where('c_login.role',
            'Teacher');
    }

    public function getSession($id)
    {
        return ESession::find($id);
    }

    public function getSessions()
    {
        return ESession::where('organization_id', \Auth::user()->organization()->id)->orderBy('session', 'ASC');
    }

    public function storeSession($input)
    {
        $session_id               = Uuid::uuid4()->getHex();
        $session                  = new ESession();
        $session->id              = $session_id;
        $session->organization_id = \Auth::user()->organization()->id;
        $session                  = $this->populateSaveValue($session, $input, array(
            'exclude' => array('_token'),
            'date'    => array('start_dt', 'end_dt')
        ));
        $save_status              = $session->save();
        $inserted_session         = ESession::find($session_id);

        return array('save_status' => $save_status, 'session' => $inserted_session);
    }

    public function updateSession($input, $id)
    {
        $session     = ESession::find($id);
        $session     = $this->populateSaveValue($session, $input, array(
            'exclude' => array('_token'),
            'date'    => array('start_dt', 'end_dt')
        ));
        $save_status = $session->save();

        return array('save_status' => $save_status, 'session' => $session);
    }

    public function deleteSession($id)
    {
        return ESession::find($id)->delete();
    }

    public function setDefaultSession($id)
    {
        \DB::table('e_session')->update(array('default' => 0));

        return ESession::find($id)->update(array('default' => 1));
    }

    public function storeSessionSubject($input)
    {
        $return_subject   = array();
        $existing_subject = array();
        $session          = ESession::find(array_get($input, 'session'));
        foreach ($session->subjects as $subject) {
            array_push($existing_subject, $subject->id);
        }

        foreach (array_get($input, 'subject') as $sub) {
            if (!in_array($sub, $existing_subject)) {
                $subject = ESubject::find($sub);
                $session->subjects()->attach($subject);
                array_push($return_subject, [$subject->id, $subject->subject_name]);
            }
        }

        return $return_subject;
    }

    public function deleteSessionSubject($id_session, $id_subject)
    {
        $session = ESession::find($id_session);
        $subject = ESubject::find($id_subject);

        return $session->subjects()->detach($subject);
    }

    public function storeSessionStudent($input)
    {
        $return_student   = array();
        $existing_student = $this->getSession(array_get($input, 'session'))->subjects()
                                 ->where('subject_id', array_get($input, 'subject'))->get()->first()->pivot->student_id;

        if ($existing_student != null) {
            $existing_student = json_decode($existing_student);
            foreach (array_get($input, 'student') as $stu) {
                if (!in_array($stu, $existing_student)) {
                    $student = DStudent::find($stu);
                    array_push($existing_student, $stu);
                    array_push($return_student, [$student->id, $student->student_name]);
                }
            }
        } else {
            $existing_student = array();
            foreach (array_get($input, 'student') as $stu) {
                $student = DStudent::find($stu);
                array_push($existing_student, $stu);
                array_push($return_student, [$student->id, $student->student_name]);
            }
        }

        $this->getSession(array_get($input, 'session'))->subjects()
             ->where('subject_id', array_get($input, 'subject'))->update(array('student_id' => json_encode($existing_student)));

        return $return_student;
    }

    public function deleteSessionStudent($id_session, $id_subject, $id_student)
    {
        $existing_student     = $this->getSession($id_session)->subjects()->where('subject_id', $id_subject)->get()->first()->pivot->student_id;
        $existing_student     = json_decode($existing_student);
        $new_existing_student = array();
        foreach ($existing_student as $es) {
            if ($es != $id_student) {
                array_push($new_existing_student, $es);
            }
        }
        $this->getSession($id_session)->subjects()->where('subject_id', $id_subject)
             ->update(array('student_id' => json_encode($new_existing_student)));

        return true;
    }

    public function getCourse($id)
    {
        return ECourse::find($id);
    }

    public function getCourses()
    {
        if (\Request::is('*academic/course/s/*')) {
            if (\Request::segment(4) == null) {
                \Redirect::to('course');
            }

            return ECourse::where('organization_id', \Auth::user()->organization()->id)
                          ->where('course_name', 'like', '%' . \Request::segment(4) . '%')
                          ->orderBy('created_at', 'DESC');
        } else {
            return ECourse::where('organization_id', \Auth::user()->organization()->id)
                          ->orderBy('created_at', 'DESC');
        }
    }

    public function storeCourse($input)
    {
        $course_id               = Uuid::uuid4()->getHex();
        $course                  = new ECourse();
        $course->id              = $course_id;
        $course->organization_id = \Auth::user()->organization()->id;
        $course                  = $this->populateSaveValue($course, $input, array(
            'exclude' => array('_token')
        ));
        $save_status             = $course->save();
        $inserted_course         = ECourse::find($course_id);

        return array('save_status' => $save_status, 'course' => $inserted_course);
    }

    public function updateCourse($input, $id)
    {
        $course      = ECourse::find($id);
        $course      = $this->populateSaveValue($course, $input, array(
            'exclude' => array('_token')
        ));
        $save_status = $course->save();

        return array('save_status' => $save_status, 'course' => $course);
    }

    public function deleteCourse($id)
    {
        return ECourse::find($id)->delete();
    }

    public function getSubject($id)
    {
        return ESubject::find($id);
    }

    public function getSubjects()
    {
        if (\Request::is('*academic/subject/s/*')) {
            if (\Request::segment(4) == null) {
                \Redirect::to('subject');
            }

            return ESubject::where('organization_id', \Auth::user()->organization()->id)
                           ->where('subject_name', 'like', '%' . \Request::segment(4) . '%')
                           ->orWhere('description', 'like', '%' . \Request::segment(4) . '%')
                           ->orWhere('code', 'like', '%' . \Request::segment(4) . '%')
                           ->orderBy('created_at', 'DESC');
        } else {
            return ESubject::where('organization_id', \Auth::user()->organization()->id)
                           ->orderBy('created_at', 'DESC');
        }
    }

    /**
     * @param        $id
     * @param string $id_session id bagi session, dalam hex
     * @return $this

    public function getSubjectsByLecturer($id, $id_session = 'all')
     *                           {
     *                           if ($id_session == 'all') {
     *                           return ESubject::where('organization_id', \Auth::user()->organization()->id)->where('lecturer_id', 'LIKE', '%' . $id . '%');
     *                           }
     *                           else {
     *                           $subjects = \DB::table('e_subject AS t1')
     *                           ->leftJoin('e_session_subject as t2', 't1.id', '=', 't2.subject_id')
     *                           ->where('t1.organization_id', \Auth::user()->organization()->id)
     *                           ->where('t1.lecturer_id', 'LIKE', '%' . $id . '%')
     *                           ->where('t2.session_id', $id_session);
     *
     * return $subjects;
     * }
     * }
     */
    /**
     * @param        $id
     * @param string $id_session id bagi session, dalam hex
     * @return $this
     */
    public function getSubjectsByStudent($id, $id_session = 'all')
    {
        if ($id_session == 'all') {
            $subjects = \DB::table('e_subject AS t1')
                           ->leftJoin('e_session_subject as t2', 't1.id', '=', 't2.subject_id')
                           ->where('t1.organization_id', \Auth::user()->organization()->id)
                           ->where('t2.student_id', 'LIKE', '%' . $id . '%');
        } else {
            $subjects = \DB::table('e_subject AS t1')
                           ->leftJoin('e_session_subject as t2', 't1.id', '=', 't2.subject_id')
                           ->leftJoin('e_subject_section as t3', 't2.subject_id', '=', 't3.subject_id')
                           ->where('t1.organization_id', \Auth::user()->organization()->id)
                           ->where('t3.student_id', 'LIKE', '%' . $id . '%')
                           ->where('t2.session_id', $id_session);
        }

        return $subjects;
    }

    public function storeSubject($input)
    {
        $subject_id               = Uuid::uuid4()->getHex();
        $subject                  = new ESubject();
        $subject->id              = $subject_id;
        $subject->organization_id = \Auth::user()->organization()->id;
        $subject->course_id       = json_encode(array_get($input, 'course_id'));
        $subject->lecturer_id     = json_encode(array_get($input, 'lecturer_id'));
        $subject                  = $this->populateSaveValue($subject, $input, array(
            'exclude' => array('_token', 'course_id', 'lecturer_id')
        ));
        $save_status              = $subject->save();
        $inserted_subject         = ESubject::find($subject_id);

        return array('save_status' => $save_status, 'subject' => $inserted_subject);
    }

    public function updateSubject($input, $id)
    {
        $subject              = ESubject::find($id);
        $subject->course_id   = json_encode(array_get($input, 'course_id'));
        $subject->lecturer_id = json_encode(array_get($input, 'lecturer_id'));
        $subject              = $this->populateSaveValue($subject, $input, array(
            'exclude' => array('_token', 'course_id', 'lecturer_id')
        ));
        $save_status          = $subject->save();

        return array('save_status' => $save_status, 'subject' => $subject);
    }

    public function deleteSubject($id)
    {
        return ESubject::find($id)->delete();
    }

    public function getSection($id)
    {
        return ESection::find($id);
    }

    public function getSections()
    {
        return ESection::where('organization_id', \Auth::user()->organization()->id)->orderBy('created_at', 'DESC');
    }

    public function getSectionStudents($section_id)
    {
        $section = ESection::find($section_id);

        return DStudent::whereIn('id', json_decode($section->student_id));
    }

    public function storeSection($input)
    {
        $section_id               = Uuid::uuid4()->getHex();
        $section                  = new ESection();
        $section->id              = $section_id;
        $section->organization_id = \Auth::user()->organization()->id;
        $section->lecturer_id     = json_encode(array_get($input, 'lecturer_id'));
        $section->student_id      = array_get($input, 'all_student_id');
        $section                  = $this->populateSaveValue($section, $input, array(
            'exclude' => array('_token', 'student_id', 'lecturer_id', 'all_student_id')
        ));
        $save_status              = $section->save();
        $inserted_section         = ESection::find($section_id);

        return array('save_status' => $save_status, 'section' => $inserted_section);
    }

    public function updateSection($input, $id)
    {
        $section     = ESection::find($id);
        $section     = $this->populateSaveValue($section, $input, array(
            'exclude' => array('_token'),
            'date'    => array('start_dt', 'end_dt')
        ));
        $save_status = $section->save();

        return array('save_status' => $save_status, 'section' => $section);
    }

    public function deleteSection($id)
    {
        return ESection::find($id)->delete();
    }

    public function getQuestions()
    {
        return GQuestion::where('created_by', \Auth::user()->login->id)->orderBy('question', 'ASC');
    }

    public function storeQuestion($input, $type)
    {
        $a = str_replace('../', '', array_get($input, 'question'));

        $question                  = new GQuestion();
        $question->id              = Uuid::uuid4()->getHex();
        $question->title           = array_get($input, 'title');
        $question->created_by      = \Auth::user()->login->id;
        $question->question_type   = $type;
        $question->question        = str_replace('roxy-user-folder', \URL::to('roxy-user-folder'), $a);
        $question->possible_answer = array_get($input, 'possible_answer');
        switch ($type) {
            case "1":
                $question->answer = array_get($input, 'answer');
                break;
            case "2":
                $question->answer = json_encode(array_get($input, 'answer'));
                break;
            case "3":
                $a = str_replace('../', '', array_get($input, 'answer'));
                $question->answer = str_replace('roxy-user-folder', \URL::to('roxy-user-folder'), $a);
                break;
            case "4":
                $question->answer = array_get($input, 'answer');
                break;
            case "5":
                $a = str_replace('../', '', array_get($input, 'answer'));
                $question->answer = str_replace('roxy-user-folder', \URL::to('roxy-user-folder'), $a);
                break;
        }
        $question->save();

        return true;
    }

    public function deleteQuestion($id)
    {
        return GQuestion::find($id)->delete();
    }
}