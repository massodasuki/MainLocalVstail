<?php
namespace App\Repositories;

use App\Models\CLogin;
use App\Models\ESection;
use App\Models\ESession;

/**
 * Class LoginRepository
 * @package App\Repositories
 *
 * Repository ini akan berfungsi untuk interaksi dengan database tujuan login dan log
 *
 */
class LoginRepository
{
    public function login($input)
    {
        if (!isset($input['password'])) {
            $input['password'] = null;
        }

        $user = CLogin::where('username', '=', $input['username'])->first();

        if ($user) {
            if ($user->status = 0) {
                return false;
            }

            if (\Auth::attempt(array('username' => $input['username'], 'password' => $input['password']))) {
                \Session::set('locale', 'en');
                if (\Auth::user()->role != 'Vital') {
                    $default = ESession::where('default', 1)->where('organization_id', \Auth::user()->organization()->id)->get();
                    if ($default->count() > 0) {
                        \Session::set('session_default', $default->first()->id);
                        \Session::set('session_name', $default->first()->session);
                        $section    = ESection::select('session_id')->where('student_id', 'like', '%' . \Auth::user()->user_id . '%')->get();
                        $session_id = array();
                        foreach ($section as $sec) {
                            array_push($session_id, $sec->session_id);
                        }
                        $session = ESession::whereIn('id', $session_id)->get();
                        \Session::set('session_all', $session);
                    } else {
                        \Session::set('session_default', null);
                    }
                }

                return true;
            }
        }

        return false;
    }

    public function getLoginByUserId($user_id)
    {
        return CLogin::find($user_id);
    }

    public function getLoginByUsername($username)
    {
        return CLogin::where('username', $username);
    }

    public function storeLogin($user_id, $input)
    {
        $login           = CLogin::find($user_id);
        $login->username = array_get($input, 'username');
        $login->password = \Hash::make(array_get($input, 'password'));
        $login->status   = 1;
        $login->save();
    }
}