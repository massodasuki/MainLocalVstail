<?php
namespace App\Repositories;

use App\Models\FOrganizations;

/**
 * Class SessionRepository
 * @package App\Repositories
 *
 * Repository ini akan berfungsi untuk interaksi dengan database bagi object session, subject, class, dan section
 *
 */
class SessionRepository
{

    public function saveOgz()
    {
        $rules                 = FOrganizations::$rules;
        $rules['contact_name'] = 'required';
        $validator             = \Validator::make(\Input::all(), $rules, FOrganizations::$messages);

        if ($validator->fails()) {
            return $validator;
        }

        $ogz           = new FOrganizations();
        $ogz->org_name = \Input::get('org_name');

        return $ogz->save();
    }
}