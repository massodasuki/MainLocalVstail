<?php
namespace App\Repositories;

use App\Libs\Vital;
use App\Models\AAssets;
use App\Models\CLogin;
use App\Models\DParent;
use App\Models\DStaff;
use App\Models\DStudent;
use App\Models\GDevice;
use Ramsey\Uuid\Uuid;

/**
 * Class ClientRepository
 * @package App\Repositories
 *
 * Repository ini akan berfungsi untuk interaksi dengan database bagi object staff, pelajar, dan ibu bapa
 *
 */
class ClientRepository
{

    use Vital;

    public function getStudent($id)
    {
        return DStudent::find($id);
    }

    public function getStudentsIn($id)
    {
        $bin_id = array();
        if ($id != null) {
            foreach ($id as $hex) {
                array_push($bin_id, $hex);
            }
        }

        return DStudent::whereIn('id', $bin_id);
    }

    public function getAllStudents()
    {
        return DStudent::where('organization_id', \Auth::user()->organization()->id);
    }

    public function getStaff($id)
    {
        return DStaff::find($id);
    }

    public function getParent($id)
    {
        return DParent::find($id);
    }

    public function storeStaff($cv, $avatar, $input)
    {
        $staff_id               = Uuid::uuid4()->getHex();
        $staff                  = new DStaff();
        $staff->id              = $staff_id;
        $staff->organization_id = \Auth::user()->organization()->id;
        $staff                  = $this->populateSaveValue($staff, $input, array(
            'exclude' => array('_token', 'cv', 'picture', 'role')
        ));

        if (count($cv) > 0) {
            $asset             = new AAssets();
            $asset->id         = $cv['asset_id'];
            $asset->for        = 'Staff CV';
            $asset->for_id     = $staff_id;
            $asset->upload_by     = \Auth::user()->login->id;
            $asset->asset_name = $cv['original_name'];
            $asset->file_size  = \Storage::disk('assets')->size('files' . DIRECTORY_SEPARATOR . $cv['new_name']);
            $asset->md5        = md5_file(env('ASSETS_STORAGE') . 'files' . DIRECTORY_SEPARATOR . $cv['new_name']);
            $asset->save();
        }

        if (count($avatar) > 0) {
            $asset             = new AAssets();
            $asset->id         = $avatar['asset_id'];
            $asset->for        = 'Staff Avatar';
            $asset->for_id     = $staff_id;
            $asset->upload_by     = \Auth::user()->login->id;
            $asset->asset_name = $avatar['original_name'];
            $asset->file_size  = \Storage::disk('assets')->size('images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->md5        = md5_file(env('ASSETS_STORAGE') . 'images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->save();
        }

        $save_status = $staff->save();

        $login           = new CLogin();
        $login->user_id  = $staff_id;
        $login->username = Uuid::uuid4()->getHex();
        $login->password = 'Not Active Yet!';
        $login->role     = array_get($input, 'role');
        $login->status   = 0;
        $login->save();

        $return = array('status' => $save_status, 'staff_id' => $staff_id, 'staff_name' => $staff->staff_name, 'email' => $staff->email);

        return $return;
    }

    public function updateStaff($staff, $cv, $avatar, $input)
    {

        if (count($cv) > 0) {
            $asset = AAssets::where('for_id', $staff->id)->where('for', 'Staff CV');
            if ($asset->count() > 0) {
                $asset->forceDelete();
            }
            $asset             = new AAssets();
            $asset->id         = $cv['asset_id'];
            $asset->for        = 'Staff CV';
            $asset->for_id     = $staff->id;
            $asset->upload_by     = \Auth::user()->login->id;
            $asset->asset_name = $cv['original_name'];
            $asset->file_size  = \Storage::disk('assets')->size('files' . DIRECTORY_SEPARATOR . $cv['new_name']);
            $asset->md5        = md5_file(env('ASSETS_STORAGE') . 'files' . DIRECTORY_SEPARATOR . $cv['new_name']);
            $asset->save();
        }

        if (count($avatar) > 0) {
            $asset = AAssets::where('for_id', $staff->id)->where('for', 'Staff Avatar');
            if ($asset->count() > 0) {
                $asset->forceDelete();
            }
            $asset             = new AAssets();
            $asset->id         = $avatar['asset_id'];
            $asset->for        = 'Staff Avatar';
            $asset->for_id     = $staff->id;
            $asset->upload_by     = \Auth::user()->login->id;
            $asset->asset_name = $avatar['original_name'];
            $asset->file_size  = \Storage::disk('assets')->size('images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->md5        = md5_file(env('ASSETS_STORAGE') . 'images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->save();
        }

        $staff = $this->populateSaveValue($staff, $input, array(
            'exclude' => array('_token', 'cv', 'picture', 'role')
        ));

        return $staff->save();
    }

    public function deleteStaff($id)
    {
        if (\Auth::user()->user_id == $id) {
            return \Redirect::to('staff')->withErrors(array('Cannot delete your own account!'));
        }

        return DStaff::find($id)->delete();
    }

    public function storeStudent($avatar, $input)
    {
        $parent_id  = null;
        $student_id = Uuid::uuid4()->getHex();

        if (count($avatar) > 0) {
            $asset             = new AAssets();
            $asset->id         = $avatar['asset_id'];
            $asset->for        = 'Student Avatar';
            $asset->for_id     = $student_id;
            $asset->upload_by     = \Auth::user()->login->id;
            $asset->asset_name = $avatar['original_name'];
            $asset->file_size  = \Storage::disk('assets')->size('images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->md5        = md5_file(env('ASSETS_STORAGE') . 'images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->save();
        }

        if (array_get($input, 'b_parent_name') != '') {
            $parent_id  = Uuid::uuid4()->getHex();
            $parent     = new DParent();
            $parent->id = $parent_id;
            $parent     = $this->populateSaveValue($parent, $input, array(
                'exclude'        => array('_token'),
                'exclude_prefix' => 'a_'
            ));
            $parent->save();
        }

        $student                  = new DStudent();
        $student->id              = $student_id;
        $student->parent_id       = $parent_id;
        $student->organization_id = \Auth::user()->organization()->id;
        $student                  = $this->populateSaveValue($student, $input, array(
            'exclude'        => array('_token', 'a_picture_id'),
            'exclude_prefix' => 'b_'
        ));

        $save_status = $student->save();

        $login           = new CLogin();
        $login->user_id  = $student_id;
        $login->username = Uuid::uuid4()->getHex();
        $login->password = 'Not Active Yet!';
        $login->role     = 'Student';
        $login->status   = 0;
        $login->save();

        $return = array('status' => $save_status, 'student_id' => $student_id, 'student_name' => $student->student_name, 'email' => $student->email);

        return $return;
    }

    public function jsonListStudent()
    {
        $keyword = '%' . \Input::get('q') . '%';
        $query   = DStudent::select(\DB::raw('id, student_name, ic_no'))
                           ->whereRaw('(student_name LIKE ? OR ic_no LIKE ? OR matric_no LIKE ?) AND organization_id = ?',
                               array($keyword, $keyword, $keyword, \Auth::user()->organization()->id));

        return $query;
    }

    public function updateStudent($avatar, $student, $input)
    {
        $parent_id = null;
        if (array_get($input, 'b_parent_name') != '') {
            if ($student->parent != null) {
                $parent = $this->getParent($student->parent->id);
            } else {
                $parent_id          = Uuid::uuid4()->getHex();
                $parent             = new DParent();
                $parent->id         = $parent_id;
                $student->parent_id = $parent_id;
            }

            $parent = $this->populateSaveValue($parent, $input, array(
                'exclude'        => array('_token'),
                'exclude_prefix' => 'a_'
            ));
            $parent->save();
        }

        if (count($avatar) > 0) {
            $asset = AAssets::where('for_id', $student->id)->where('for', 'Student Avatar');
            if ($asset->count() > 0) {
                $asset->forceDelete();
            }
            $asset             = new AAssets();
            $asset->id         = $avatar['asset_id'];
            $asset->for        = 'Student Avatar';
            $asset->for_id     = $student->id;
            $asset->upload_by     = \Auth::user()->login->id;
            $asset->asset_name = $avatar['original_name'];
            $asset->file_size  = \Storage::disk('assets')->size('images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->md5        = md5_file(env('ASSETS_STORAGE') . 'images' . DIRECTORY_SEPARATOR . $avatar['new_name']);
            $asset->save();
        }

        $student = $this->populateSaveValue($student, $input, array(
            'exclude'        => array('_token', 'a_picture_id'),
            'exclude_prefix' => 'b_'
        ));

        return $student->save();
    }

    //get client information
    public function getDevices()
    {
        return GDevice::where('organization_id', \Auth::user()->organization()->id)
                      ->orderBy('created_at', 'DESC');
    }

    //get client information
    public function getDevice($id)
    {
        return GDevice::find($id);
    }

    public function updateDevice($input, $id)
    {
        $device             = GDevice::find($id);
        $device->subject_id = json_encode(array_get($input, 'subjects'));
        $device->location   = array_get($input, 'location');
        $save_status        = $device->save();

        return array('save_status' => $save_status, 'device' => $device);
    }
}