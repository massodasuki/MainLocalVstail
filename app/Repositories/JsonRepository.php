<?php
namespace App\Repositories;

use App\Libs\DxGrid\DxGrid;
use App\Models;
use Carbon\Carbon;

/**
 * Class JsonRepository
 * @package App\Repositories
 *
 * Repository ini akan berfungsi untuk interaksi dengan database bagi mendapatkan json data untuk di supply kepada grid
 *
 */
class JsonRepository
{

    public function optionGrid()
    {
        if (\Auth::user()->role == 'Vital') {
            $category = \Auth::user()->user_id;
        } else {
            $category = \Auth::user()->organization()->id;
        }
        $query      = Models\AOption::select(\DB::raw('id, category, item'))->where('vital_or_organization', $category);
        $optionGrid = new DxGrid($query);

        return $optionGrid->result();
    }

    public function staffGrid()
    {
        $option = array();

        $query = Models\DStaff::select(array(
            \DB::raw('d_staff.id, d_staff.staff_name, c_login.role, a_option.item, d_staff.email, d_staff.employee_no, d_staff.tel_no')
        ))
                              ->leftJoin('a_option', function ($join) {
                                  $join->on('d_staff.position_id', '=', 'a_option.id');
                              })
                              ->leftJoin('c_login', function ($join) {
                                  $join->on('d_staff.id', '=', 'c_login.user_id');
                              })
                              ->whereNull('d_staff.deleted_at');

        $staff = new DxGrid($query, $option);

        return $staff->result();
    }

    public function studentGrid()
    {
        $query   = Models\DStudent::select(array(\DB::raw('id, student_name, matric_no, ic_no, email, hp_no')))->whereNull('deleted_at');
        $student = new DxGrid($query);

        return $student->result();
    }

    public function subjectGrid()
    {
        $subject = Models\ESubject::select(array(\DB::raw('id, subject_name, code, description')))->whereNull('deleted_at');
        $DxGrid  = new DxGrid($subject);

        return $DxGrid->result();
    }
}