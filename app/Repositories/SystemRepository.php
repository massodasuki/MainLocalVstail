<?php
namespace App\Repositories;

use App\Libs\Vital;
use App\Models\AConfig;
use App\Models\AOption;
use Ramsey\Uuid\Uuid;

/**
 * Class SystemRepository
 * @package App\Repositories
 *
 * Repository ini akan berfungsi untuk interaksi dengan database bagi menguruskan system property
 *
 */
class SystemRepository
{
    use Vital;

    public function getConfig($category)
    {
        $config = AConfig::where('vital_or_organization', $category)
                         ->get();

        return $config;
    }

    public function updateConfig($input, $category)
    {
        foreach ($input as $key => $value) {
            $config        = AConfig::where('param', $key)->where('vital_or_organization', $category)->first();
            $config->value = $value;
            $config->save();
        }
    }

    public function getOption($category, $vital_or_organization)
    {
        $config = AOption::select(\DB::raw('id, item'))
                         ->where('category', $category)
                         ->where('vital_or_organization', $vital_or_organization)
                         ->get();

        return $config;
    }

    public function storeOption($input, $category)
    {
        $option                        = new AOption();
        $option->id                    = Uuid::uuid4()->getHex();
        $option->category              = array_get($input, 'category');
        $option->item                  = array_get($input, 'item');
        $option->vital_or_organization = $category;
        $option->save();
    }

    public function editOption($id)
    {
        $option = AOption::find($id);

        return $option;
    }

    public function updateOption($id, $input)
    {
        $option           = AOption::find($id);
        $option->category = array_get($input, 'category');
        $option->item     = array_get($input, 'item');
        $option->save();
    }

    public function deleteOption($id)
    {
        AOption::find($id)->delete();
    }
}