<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;


class ESection extends \Eloquent
{
    protected $table = 'e_section';

    public function subject()
    {
        return ESubject::where('id', $this->subject_id)->first();
    }

    public function lecturers()
    {
        return DStaff::whereIn('id', json_decode($this->lecturer_id))->get();
    }

    public function session()
    {
        return ESession::where('id', $this->session_id)->first();
    }

    public function students()
    {
        return DStudent::whereIn('id', json_decode($this->student_id))->get();
    }
}