<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class FContentVideo extends \Eloquent
{
    use SoftDeletes;

    protected $fillable = ['mandatory'];
    
    protected $table = 'f_content_video';

    public function subject()
    {
        return $this->hasOne('App\Models\ESubject', 'id', 'subject_id');
    }

    public function thumbnail()
    {
        return $this->hasOne('App\Models\AAssets', 'for_id', 'id')->where('for', 'Content Thumbnail');
    }

    public function asset()
    {
        return $this->hasOne('App\Models\AAssets', 'for_id', 'id')->where('for', 'Content Video Original');
    }
}