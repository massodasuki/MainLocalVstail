<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;


class FLessonHistory extends \Eloquent
{
    protected $table = 'f_lesson_history';

    public function student()
    {
        return $this->hasOne('App\Models\DStudent', 'id', 'student_id');
    }
}