<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ESubject extends \Eloquent
{
    use SoftDeletes;
    protected $table = 'e_subject';

    public function courses()
    {
        return ECourse::whereIn('id', json_decode($this->course_id));
    }

    public function lecturers()
    {
        $sections    = ESection::where('session_id', \Session::get('session_default'))
                               ->where('subject_id', $this->id)->get();
        $lecturer_id = array();
        foreach ($sections as $sec) {
            $lecturer_id = array_merge($lecturer_id, json_decode($sec->lecturer_id));
        }

        return DStaff::whereIn('id', $lecturer_id);
    }

    public function contentVideos()
    {
        return FContentVideo::where('subject_id', $this->id);
    }

    public function contentOthers()
    {
        return FContentOther::where('subject_id', $this->id);
    }
}