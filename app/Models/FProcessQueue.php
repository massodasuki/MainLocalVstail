<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;


class FProcessQueue extends \Eloquent
{
    protected $table = 'f_process_queue';

    public function asset()
    {
        return $this->hasOne('App\Models\AAssets', 'id', 'asset_id');
    }
}