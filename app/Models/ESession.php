<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ESession extends \Eloquent
{
    use SoftDeletes;
    protected $table    = 'e_session';
    protected $fillable = ['default'];

    public function subjects()
    {
        return $this->belongsToMany('App\Models\ESubject', 'e_session_subject', 'session_id', 'subject_id')->withPivot('student_id', 'content_id');
    }
}