<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class GQuestion extends \Eloquent
{
    use SoftDeletes;
    protected $table = 'g_question';

}