<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class AAssets extends \Eloquent
{
    use SoftDeletes;
    protected $table = 'a_assets';

    public function uploadBy()
    {
        return $this->hasOne('App\Models\CLogin', 'user_id', 'upload_by');
    }
}