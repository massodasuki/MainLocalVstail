<?php

namespace App\Models;

use App\Libs\Vital;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class CLogin extends \Eloquent implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    use Vital;

    public $table      = 'c_login';

    public $primaryKey = 'user_id';

    public function login()
    {
        if ($this->role == 'Vital') {
            return $this->hasOne('App\Models\BVital', 'id');
        } else if ($this->role == 'Super Admin' || $this->role == 'Teacher') {
            return $this->hasOne('App\Models\DStaff', 'id');
        } else if ($this->role == 'Student') {
            return $this->hasOne('App\Models\DStudent', 'id');
        }
    }

    public function log()
    {
        return $this->hasMany('App\Models\CLoginLog', 'login_id', 'user_id');
    }

    public function organization()
    {
        $organization = null;
        if ($this->role == 'Super Admin' || $this->role == 'Teacher') {
            $organization = DClient::find(DStaff::find($this->user_id)->organization_id);
        } else if ($this->role == 'Student') {
            $organization = DClient::find(DStudent::find($this->user_id)->organization_id);
        }

        return $organization;
    }

    public function getProfilePicture()
    {
        $pic = \URL::to('assets/images/no_photo.png');
        if ($this->role == 'Super Admin' || $this->role == 'Teacher') {
            $pic = \URL::to('assets/images/' . $this->getFile(DStaff::find($this->user_id)->avatar));
        } else if ($this->role == 'Student') {
            $pic = \URL::to('assets/images/' . $this->getFile(DStudent::find($this->user_id)->avatar));
        }

        if ($pic == null) {
            $pic = 'no_photo.png';
        }

        return $pic;
    }

    public function getProfileName()
    {
        $name = null;
        if ($this->role == 'Super Admin' || $this->role == 'Teacher') {
            $name = DStaff::find($this->user_id)->staff_name;
        } else if ($this->role == 'Student') {
            $name = DStudent::find($this->user_id)->student_name;
        }

        return $name;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     *
     * @return void
     */
    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }
}