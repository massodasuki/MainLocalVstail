<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */

namespace App\Models;


class FContentOther extends \Eloquent
{
    protected $table = 'f_content_other';

    public function subject()
    {
        return $this->hasOne('App\Models\ESubject', 'id', 'subject_id');
    }
}