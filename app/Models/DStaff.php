<?php
/**
 * Created by PhpStorm.
 * User: A.Fattah
 * Date: 06-Jul-15
 * Time: 1:46 PM
 */
namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class DStaff extends \Eloquent
{
    use SoftDeletes;
    protected $table = 'd_staff';

    public function position()
    {
        return $this->hasOne('App\Models\AOption', 'id', 'position_id');
    }

    public function role()
    {
        return CLogin::find($this->id)->role;
    }

    public function login()
    {
        return $this->hasOne('App\Models\CLogin', 'user_id', 'id');
    }

    public function avatar()
    {
        return $this->hasOne('App\Models\AAssets', 'for_id', 'id')->where('for', 'Staff Avatar');
    }

    public function cv()
    {
        return $this->hasOne('App\Models\AAssets', 'for_id', 'id')->where('for', 'Staff CV');
    }

    public function fullAddress()
    {
        $address = '';
        if ($this->address_1 != null OR $this->address_1 != '') {
            $address .= $this->address_1 . ', ';
        }
        if ($this->address_2 != null OR $this->address_2 != '') {
            $address .= $this->address_2 . ', ';
        }
        if ($this->address_3 != null OR $this->address_3 != '') {
            $address .= $this->address_3 . ', ';
        }
        if ($this->postcode != null OR $this->postcode != '') {
            $address .= $this->postcode . ', ';
        }
        if ($this->town != null OR $this->town != '') {
            $address .= $this->town . ', ';
        }
        if ($this->state != null OR $this->state != '') {
            $address .= $this->state . ', ';
        }
        if ($this->country != null OR $this->country != '') {
            $address .= $this->country;
        }

        if (substr($address, -2) == ', ') {
            $address = substr($address, 0, -2);
        }

        return $address;
    }
}